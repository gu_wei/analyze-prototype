CREATE EXTENSION postgis;

CREATE TABLE itinerary_analyze (
  analyze_id 	BIGINT NOT NULL,
  itinerary_id VARCHAR(32) NOT NULL,
  reservation_package JSONB NOT NULL,
  result JSONB NOT NULL,
  performance JSONB NOT NULL
);

CREATE TABLE wcities_city (
  city_id  	VARCHAR(16) NOT NULL,
  name      VARCHAR(128) NOT NULL,
  location  geography(POINT,4326) NOT NULL
);

CREATE TABLE shoretrip_port (
  PORT  	  VARCHAR(3) NOT NULL,
  name      VARCHAR(128) NOT NULL,
  location  geography(POINT,4326) NOT NULL
);