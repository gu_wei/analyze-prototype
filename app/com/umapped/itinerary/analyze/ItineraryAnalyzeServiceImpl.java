package com.umapped.itinerary.analyze;

import com.mapped.publisher.parse.schemaorg.ReservationPackage;
import com.umapped.itinerary.analyze.adapters.ReservationPackageItineraryAdapter;
import com.umapped.itinerary.analyze.log.ItineraryAnalyzeServiceLog;
import com.umapped.itinerary.analyze.model.Itinerary;
import com.umapped.itinerary.analyze.segment.SegmentDetector;
import com.umapped.itinerary.analyze.segment.TripSegment;
import org.apache.commons.lang3.time.StopWatch;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by wei on 2017-03-29.
 */
@Singleton public class ItineraryAnalyzeServiceImpl
    implements ItineraryAnalyzeService {
  private ReservationPackageItineraryAdapter reservationPackageItineraryAdapter;
  private SegmentDetector segmentDetector;
  private ItineraryAnalyzer itineraryAnalyzer;
  private ItineraryAnalyzeServiceLog serviceLog;
  private ItineraryLocationEnhancer itineraryLocationEnhancer;

  @Inject
  public ItineraryAnalyzeServiceImpl(ReservationPackageItineraryAdapter rpAdapter,
                                     ItineraryLocationEnhancer itineraryLocationEnhancer,
                                     SegmentDetector segmentDetector,
                                     ItineraryAnalyzer itineraryAnalyzer,
                                     ItineraryAnalyzeServiceLog serviceLog) {
    this.reservationPackageItineraryAdapter = rpAdapter;
    this.itineraryLocationEnhancer = itineraryLocationEnhancer;
    this.segmentDetector = segmentDetector;
    this.itineraryAnalyzer = itineraryAnalyzer;
    this.serviceLog = serviceLog;
  }

  @Override public ItineraryAnalyzeResult analyze(ReservationPackage reservationPackage) {
    StopWatch sw = new StopWatch();
    sw.start();
    Itinerary itinerary = reservationPackageItineraryAdapter.adapt(reservationPackage);


    long rpToItineraryTime = sw.getTime();

    itineraryLocationEnhancer.enhanceItineraryLocation(itinerary);

    long locEnhanceTime = sw.getTime();

    List<TripSegment> segments = segmentDetector.analyze(itinerary);

    long segmentDetectTime = sw.getTime();


    ItineraryAnalyzeResult analyzeResult = itineraryAnalyzer.analyze(itinerary.getUmItineraryId(), segments);

    long analyzeTime = sw.getTime();
    sw.stop();

    log(reservationPackage, analyzeResult, rpToItineraryTime, locEnhanceTime, segmentDetectTime, analyzeTime);

    return analyzeResult;
  }

  private void log(ReservationPackage rp, ItineraryAnalyzeResult result, long... time) {
    Map<String, Long> performance = new HashMap<>();
    performance.put("RPToItinerary", time[0]);
    performance.put("LocationEnhance", time[1]-time[0]);
    performance.put("SegmentDetect", time[2]-time[1]);
    performance.put("Analyze", time[3] - time[2]);
    serviceLog.log(rp, result, performance);
  }
}
