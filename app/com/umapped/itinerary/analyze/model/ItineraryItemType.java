package com.umapped.itinerary.analyze.model;

/**
 * Created by wei on 2017-03-01.
 */
public enum ItineraryItemType {
  Flight,
  Accommodation,
  Activity,
  Cruise,
  CruiseStop,
  LongTransfer,
  ShortTransfer
}
