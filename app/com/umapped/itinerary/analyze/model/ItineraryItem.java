package com.umapped.itinerary.analyze.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

import java.time.Duration;
import java.time.ZonedDateTime;

/**
 * Created by wei on 2017-02-28.
 */
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "@type")
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class ItineraryItem {
  private ItineraryItemType type;
  private ZonedDateTime startDateTime;
  private ZonedDateTime endDateTime;

  private Location startLocation;
  private Location endLocation;

  private String reservationType;
  private String name;
  private String umId;


  public Location getStartLocation() {
    return startLocation;
  }

  public ItineraryItem setStartLocation(Location startLocation) {
    this.startLocation = startLocation;
    return this;
  }

  public Location getEndLocation() {
    return endLocation;
  }

  public ItineraryItem setEndLocation(Location endLocation) {
    this.endLocation = endLocation;
    return this;
  }

  public ItineraryItemType getType() {
    return type;
  }

  public ItineraryItem setType(ItineraryItemType type) {
    this.type = type;
    return this;
  }

  public ZonedDateTime getStartDateTime() {
    return startDateTime;
  }

  public ItineraryItem setStartDateTime(ZonedDateTime startDateTime) {
    this.startDateTime = startDateTime;
    return this;
  }

  public ZonedDateTime getEndDateTime() {
    return endDateTime;
  }

  public ItineraryItem setEndDateTime(ZonedDateTime endDateTime) {
    this.endDateTime = endDateTime;
    return this;
  }

  public String getReservationType() {
    return reservationType;
  }

  public ItineraryItem setReservationType(String reservationType) {
    this.reservationType = reservationType;
    return this;
  }

  public String getName() {
    return name;
  }

  public ItineraryItem setName(String name) {
    this.name = name;
    return this;
  }

  public String getUmId() {
    return umId;
  }

  public ItineraryItem setUmId(String umId) {
    this.umId = umId;
    return this;
  }

  public int compareByStartTime(ItineraryItem other) {
    ZonedDateTime thisStartTime = this.startDateTime;
    ZonedDateTime thatStartTime = other.startDateTime;
    if (thisStartTime.equals(thatStartTime)) {
      return 0;
    } else if (thisStartTime.isBefore(thatStartTime)) {
      return -1;
    } else {
      return 1;
    }
  }

  public long getDuration() {
    if (startDateTime != null && endDateTime != null) {
      return Duration.between(startDateTime, endDateTime).toHours();
    } else {
      return 0;
    }
  }
}
