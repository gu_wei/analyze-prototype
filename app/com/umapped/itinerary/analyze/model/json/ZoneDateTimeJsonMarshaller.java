package com.umapped.itinerary.analyze.model.json;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import org.apache.commons.lang3.StringUtils;

import java.io.IOException;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

/**
 * Created by wei on 2017-03-01.
 */
public class ZoneDateTimeJsonMarshaller {
  public static class ZoneDateTimeDeserializer extends StdDeserializer<ZonedDateTime> {
    public ZoneDateTimeDeserializer() {
      super(ZonedDateTime.class);
    }

    public ZoneDateTimeDeserializer(Class<ZonedDateTime> vc) {
      super(vc);
    }

    @Override public ZonedDateTime deserialize(JsonParser jsonParser, DeserializationContext deserializationContext)
        throws IOException, JsonProcessingException {
      String text = jsonParser.getText();
      if (StringUtils.isEmpty(text)) {
        return null;
      } else {
        return ZonedDateTime.parse(text, DateTimeFormatter.ISO_DATE_TIME);
      }
    }
  }

  /**
   * Created by wei on 2017-02-28.
   */
  public static class ZoneDateTimeSerializer
      extends StdSerializer<ZonedDateTime> {

    public ZoneDateTimeSerializer() {
      super(ZonedDateTime.class);
    }

    public ZoneDateTimeSerializer(Class<ZonedDateTime> t) {
      super(t);
    }

    @Override
    public void serialize(ZonedDateTime datetime, JsonGenerator jsonGenerator, SerializerProvider serializerProvider)
        throws IOException {
      if (datetime != null) {
        jsonGenerator.writeString(datetime.format(DateTimeFormatter.ISO_DATE_TIME));
      }
    }
  }
}
