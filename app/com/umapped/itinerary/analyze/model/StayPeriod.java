package com.umapped.itinerary.analyze.model;

import java.time.LocalDate;

/**
 * Created by wei on 2017-03-10.
 *
 *  Date Range with Location
 */
public class StayPeriod implements Comparable {

 private LocalDate start;
 private LocalDate end;

  public StayPeriod(LocalDate start, LocalDate end) {
    this.start = start;
    this.end = end;
  }

  public LocalDate getStart() {
    return start;
  }

  public LocalDate getEnd() {
    return end;
  }

  @Override public int compareTo(Object other) {
    return getStart().compareTo(((StayPeriod) other).getStart());
  }

  public String toString() {
    return start.toString() + " - " + end.toString();
  }
}
