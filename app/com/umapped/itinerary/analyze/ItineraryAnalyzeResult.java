package com.umapped.itinerary.analyze;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.umapped.itinerary.analyze.segment.TripSegmentAnalyzeResult;

import java.util.EnumMap;
import java.util.List;

/**
 * Created by wei on 2017-03-29.
 */
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class ItineraryAnalyzeResult {
  private String itineraryId;

  private EnumMap<TripFeature, Object> tripFetures;

  private List<TripSegmentAnalyzeResult> segmentResults;

  public String getItineraryId() {
    return itineraryId;
  }

  public ItineraryAnalyzeResult setItineraryId(String itineraryId) {
    this.itineraryId = itineraryId;
    return this;
  }

  public EnumMap<TripFeature, Object> getTripFetures() {
    return tripFetures;
  }

  public ItineraryAnalyzeResult setTripFetures(EnumMap<TripFeature, Object> tripFetures) {
    this.tripFetures = tripFetures;
    return this;
  }

  public List<TripSegmentAnalyzeResult> getSegmentResults() {
    return segmentResults;
  }

  public ItineraryAnalyzeResult setSegmentResults(List<TripSegmentAnalyzeResult> segmentResults) {
    this.segmentResults = segmentResults;
    return this;
  }
}
