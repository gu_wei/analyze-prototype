package com.umapped.itinerary.analyze.geo;

import com.google.maps.GeoApiContext;

/**
 * Created by wei on 2017-02-23.
 */
public class GoogleMapApiSupport {
    private static String GEOCODING_API_KEY = "AIzaSyA6j8P8k2M3v0eBJKAlOMiNf9npuQQZALI";
    private static String DISTANCE_API_KEY = "AIzaSyAUOI8FvI1Dbzc-pFX5PnnbZJzzOq-6M8I";
    private static String TIMEZONE_API_KEY = "AIzaSyCCcne9NOylG-S-2yb4TnGsZc7v8vbJcwQ";


    public static GeoApiContext getGeoCodingApiContext() {
        GeoApiContext context = new GeoApiContext();
        context.setApiKey(GEOCODING_API_KEY);
        return context;
    }

    public static GeoApiContext getTimezoneApiContext() {
        GeoApiContext context = new GeoApiContext();
        context.setApiKey(TIMEZONE_API_KEY);
        return context;
    }

    public static GeoApiContext getDistanceApiContext() {
        GeoApiContext context = new GeoApiContext();
        context.setApiKey(DISTANCE_API_KEY);
        return context;
    }
}
