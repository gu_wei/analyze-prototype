package com.umapped.itinerary.analyze.geo;

import com.google.maps.GeoApiContext;
import com.google.maps.PendingResult;
import com.google.maps.TimeZoneApi;
import com.google.maps.model.LatLng;

import java.util.TimeZone;

/**
 * Created by wei on 2017-02-23.
 */
public class TimezoneTest {
    public static void main(String[] args) throws Exception {
        GeoApiContext context = GoogleMapApiSupport.getTimezoneApiContext();
        LatLng location = new LatLng(43.677223, -79.630554);
        PendingResult<TimeZone> result = TimeZoneApi.getTimeZone(context, location);
        TimeZone tz = result.await();
        System.out.println(tz);
    }
}
