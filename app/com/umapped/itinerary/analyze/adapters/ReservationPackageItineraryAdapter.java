package com.umapped.itinerary.analyze.adapters;

import com.mapped.publisher.parse.schemaorg.*;
import com.umapped.itinerary.analyze.model.Itinerary;
import com.umapped.itinerary.analyze.model.ItineraryItem;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Created by wei on 2017-03-06.
 */
public class ReservationPackageItineraryAdapter {

  private static Map<Class<? extends Reservation>, ReservationItineraryItemAdapter> adapters = new HashMap<>();


  static {
    adapters.put(FlightReservation.class, new FlightReservationAdapter());
    adapters.put(LodgingReservation.class, new AccommodationAdapter());
    adapters.put(CruiseShipReservation.class, new CruiseReservationAdapter());
    adapters.put(TrainReservation.class, new TrainReservationAdapter());
    adapters.put(ActivityReservation.class, new ActivityReservationAdapter());
    adapters.put(FoodEstablishmentReservation.class, new FoodEstablishmentReservationReservationAdapter());
    adapters.put(RentalCarReservation.class, new RentalCarReservationAdapter());
    adapters.put(BusReservation.class, new BusReservationAdapter());
    adapters.put(TransferReservation.class, new TransferReservationAdapter());
    adapters.put(TaxiReservation.class, new TaxiReservationAdapter());
    adapters.put(EventReservation.class, new EventReservationReservationAdapter());
  }

  public Itinerary adapt(ReservationPackage reservationPackage) {
    Itinerary itinerary = new Itinerary(reservationPackage.umId);

    List<ItineraryItem> items = reservationPackage.subReservation.stream()
                                                                 .map(r -> adapters.get(r.getClass()).adapt(r))
                                                                 .collect(Collectors.toList());
    itinerary.setItems(items);
    return itinerary;
  }
}
