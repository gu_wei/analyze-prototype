package com.umapped.itinerary.analyze.adapters;

import com.mapped.publisher.parse.schemaorg.RentalCarReservation;
import com.mapped.publisher.parse.schemaorg.Reservation;
import com.umapped.itinerary.analyze.model.ItineraryItemType;
import com.umapped.itinerary.analyze.model.Location;

/**
 * Created by wei on 2017-03-22.
 */
public class RentalCarReservationAdapter extends AbstractReservationItinearyItemAdapter {
  @Override protected ItineraryItemType getItemType(Reservation reservation) {
    return ItineraryItemType.LongTransfer;
  }

  @Override protected Location getStartLocation(Reservation reservation) {
    RentalCarReservation r = (RentalCarReservation) reservation;
    return getLocationFromPlace(r.pickupLocation);
  }

  @Override protected Location getEndLocation(Reservation reservation) {
    RentalCarReservation r = (RentalCarReservation) reservation;
    return getLocationFromPlace(r.dropoffLocation);
  }
}
