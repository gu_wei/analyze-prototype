package com.umapped.itinerary.analyze.adapters;

import com.mapped.publisher.parse.schemaorg.BusReservation;
import com.mapped.publisher.parse.schemaorg.Reservation;
import com.umapped.itinerary.analyze.model.Location;

/**
 * Created by wei on 2017-03-22.
 */
public class BusReservationAdapter extends TrainReservationAdapter {
  @Override protected Location getStartLocation(Reservation reservation) {
    BusReservation r = (BusReservation) reservation;
    return getLocationFromPlace(r.reservationFor.departureBusStop);
  }

  @Override protected Location getEndLocation(Reservation reservation) {
    BusReservation r = (BusReservation) reservation;
    return getLocationFromPlace(r.reservationFor.arrivalBusStop);
  }
}
