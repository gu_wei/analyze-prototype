package com.umapped.itinerary.analyze.adapters;

import com.mapped.publisher.parse.schemaorg.EventReservation;
import com.mapped.publisher.parse.schemaorg.Reservation;
import com.umapped.itinerary.analyze.model.Location;

/**
 * Created by wei on 2017-03-22.
 */
public class EventReservationReservationAdapter
    extends ActivityReservationAdapter {
  @Override protected Location getStartLocation(Reservation reservation) {
    EventReservation r = (EventReservation) reservation;
    return getLocationFromPlace(r.reservationFor.location);
  }

  @Override protected Location getEndLocation(Reservation reservation) {
    return this.getStartLocation(reservation);
  }
}
