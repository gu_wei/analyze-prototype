package com.umapped.itinerary.analyze.adapters;

import com.mapped.publisher.parse.schemaorg.LodgingReservation;
import com.mapped.publisher.parse.schemaorg.PostalAddress;
import com.mapped.publisher.parse.schemaorg.Reservation;
import com.umapped.itinerary.analyze.model.ItineraryItemType;
import com.umapped.itinerary.analyze.model.Location;

/**
 * Created by wei on 2017-03-07.
 */
public class AccommodationAdapter extends AbstractReservationItinearyItemAdapter {
  @Override protected ItineraryItemType getItemType(Reservation reservation) {
    return ItineraryItemType.Accommodation;
  }

  @Override protected Location getStartLocation(Reservation reservation) {
    LodgingReservation r = (LodgingReservation) reservation;
    Location location = new Location();

    PostalAddress address = r.reservationFor.address;

    location.fromPostalAddress(address);
    location.setUmPoidId(r.reservationFor.umId);
    location.setDescription(r.reservationFor.name);
    return location;
  }

  @Override protected Location getEndLocation(Reservation reservation) {
    return this.getStartLocation(reservation);
  }
}
