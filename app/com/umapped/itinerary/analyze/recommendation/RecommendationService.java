package com.umapped.itinerary.analyze.recommendation;

import com.umapped.itinerary.analyze.ItineraryAnalyzeResult;

/**
 * Created by wei on 2017-03-31.
 */
public interface RecommendationService {
  ItineraryRecommendation recommend(ItineraryAnalyzeResult result);
}
