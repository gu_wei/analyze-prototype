package com.umapped.itinerary.analyze.recommendation;

import com.umapped.itinerary.analyze.model.StayPeriod;
import com.umapped.itinerary.analyze.segment.TripSegmentAnalyzeResult;

import java.time.LocalDate;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 * Created by wei on 2017-03-31.
 */
public class TripSegmentRecommendation {
  private TripSegmentAnalyzeResult segmentResult;

  private Map<StayPeriod, List<RecommendedItem>> activities;
  private Map<StayPeriod, List<RecommendedItem>> hotels;
  private Map<StayPeriod, List<RecommendedItem>> guides;

  public Map<StayPeriod, List<RecommendedItem>> getActivities() {
    return activities;
  }

  public synchronized void addHotel(LocalDate startDate, LocalDate endDate, List<RecommendedItem> hotelList) {
    if (hotels == null) {
      hotels = new TreeMap<>();
    }
    hotels.put(new StayPeriod(startDate, endDate), hotelList);
  }

  public synchronized void addGuide(LocalDate startDate, LocalDate endDate, List<RecommendedItem> guideList) {
    if (guides == null) {
      guides = new TreeMap<>();
    }
    guides.put(new StayPeriod(startDate, endDate), guideList);

  }

  public synchronized void addActivity(LocalDate startDate, LocalDate endDate, List<RecommendedItem> activityList) {
    if (activities == null) {
      activities = new TreeMap<>();
    }
    activities.put(new StayPeriod(startDate, endDate), activityList);
  }


  public Map<StayPeriod, List<RecommendedItem>> getHotels() {
    return hotels;
  }

  public Map<StayPeriod, List<RecommendedItem>> getGuides() {
    return guides;
  }

  public TripSegmentRecommendation setSegmentResult(TripSegmentAnalyzeResult segmentResult) {
    this.segmentResult = segmentResult;
    return this;
  }
}
