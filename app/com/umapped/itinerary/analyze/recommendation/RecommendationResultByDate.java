package com.umapped.itinerary.analyze.recommendation;

import com.umapped.itinerary.analyze.model.StayPeriod;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 * Created by wei on 2017-04-06.
 */
public class RecommendationResultByDate {
  TreeMap<LocalDate, List<RecommendedItem>> recommendationMap;

  public RecommendationResultByDate() {
    recommendationMap = new TreeMap<>();
  }

  public TreeMap<LocalDate, List<RecommendedItem>> getRecommendationMap() {
    return recommendationMap;
  }

  public void add(TripSegmentRecommendation trs) {
    add(trs.getActivities());
    add(trs.getHotels());
  }

  public void add(Map<StayPeriod, List<RecommendedItem>> items) {
    if (items == null) {
      return;
    }
    for (StayPeriod p : items.keySet()) {
      List<RecommendedItem> itemList = items.get(p);
      List<RecommendedItem> itemsInMap = recommendationMap.get(p.getStart());
      if (itemsInMap == null) {
        itemsInMap = new ArrayList<>();
        recommendationMap.put(p.getStart(), itemsInMap);
      }
      itemsInMap.addAll(itemList);
    }
  }
}
