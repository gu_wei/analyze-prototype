package com.umapped.itinerary.analyze.actor;

import akka.actor.ActorSystem;
import com.google.inject.Provider;
import com.google.inject.Singleton;

/**
 * Created by wei on 2017-03-22.
 */
@Singleton
public class ActorSystemProvider implements Provider<ActorSystem> {
  private ActorSystem system = ActorSystem.create();
  @Override public ActorSystem get() {
    return system;
  }
}
