package com.umapped.itinerary.analyze.webservice;

import com.umapped.itinerary.analyze.location.geonames.GeoNamesServiceException;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

/**
 * Created by wei on 2017-03-30.
 */
public abstract class WebServiceRequestHandler {
  private static final Logger LOG = LoggerFactory.getLogger(WebServiceRequestHandler.class);

  abstract protected String getServerUrl();
  //abstract protected
  public <R> R get(String endpoint, WebAPIServiceRequest serviceRequest, Function<InputStream, R> decoder) {
    String url = getServerUrl() + endpoint + "?" + serviceRequest.buildQueryString();
    LOG.debug("Request url: " + url);
    HttpGet request = new HttpGet(url);

    try (CloseableHttpClient client = HttpClients.createDefault();
         CloseableHttpResponse response = client.execute(request)) {
      HttpEntity entity = response.getEntity();

      return decoder.apply(entity.getContent());
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }

  public <R> R post(String endpoint, String payload, Function<InputStream, R> decoder) {
    String url = getServerUrl() + endpoint ;
    LOG.debug("Request url: " + url);
    HttpPost request = new HttpPost(url);
    try {
      StringEntity requestEntity = new StringEntity(payload);
      request.setEntity(requestEntity);
    } catch (UnsupportedEncodingException e) {
      LOG.error("fail to create string entity", e);
      throw new RuntimeException(e);
    }
    request.setHeader("Content-Type", "text/xml");
    try (CloseableHttpClient client = HttpClients.createDefault();
         CloseableHttpResponse response = client.execute(request)) {
      Header[] headers = response.getAllHeaders();
      for (Header h : headers) {
        LOG.debug("Header: " + h.getName() + " - " + h.getValue());
      }
      HttpEntity entity = response.getEntity();

      return decoder.apply(entity.getContent());
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }
}
