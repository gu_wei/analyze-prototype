package com.umapped.itinerary.analyze;

import akka.actor.ActorSystem;
import com.google.inject.AbstractModule;
import com.umapped.itinerary.analyze.actor.ActorSystemProvider;
import com.umapped.itinerary.analyze.location.LocationEnhancer;
import com.umapped.itinerary.analyze.location.TimeZoneLookupService;
import com.umapped.itinerary.analyze.location.LocationLookupService;
import com.umapped.itinerary.analyze.location.geonames.GeoNamesTimeZoneLookupService;
import com.umapped.itinerary.analyze.location.google.GoogleLocationLookupSevice;

/**
 * Created by wei on 2017-03-22.
 */
public class WebServiceModule extends AbstractModule {
  @Override
  protected void configure() {
    bind(TimeZoneLookupService.class).to(GeoNamesTimeZoneLookupService.class);
    bind(LocationLookupService.class).to(GoogleLocationLookupSevice.class);
    bind(ActorSystem.class).toProvider(ActorSystemProvider.class);
    try {
      bind(LocationEnhancer.class).toConstructor(LocationEnhancer.class.getConstructor(TimeZoneLookupService.class));
    } catch (Exception e) {
      e.printStackTrace();
    }
  }
}
