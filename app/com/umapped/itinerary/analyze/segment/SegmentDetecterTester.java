package com.umapped.itinerary.analyze.segment;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.inject.Guice;
import com.google.inject.Inject;
import com.google.inject.Injector;
import com.umapped.itinerary.analyze.ItineraryAnalyzeModule;
import com.umapped.itinerary.analyze.model.Itinerary;
import com.umapped.itinerary.analyze.model.ItineraryItem;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import java.util.Set;

/**
 * Created by wei on 2017-03-02.
 */
public class SegmentDetecterTester {

  private static String DATA_ROOT = "/Users/wei/DevResources/data/trips/";
  private static String SEGMENT_ROOT = DATA_ROOT + "segment/";
  private static String ITINERARY_ROOT = DATA_ROOT + "itinerary/";
  private static String DATE = "2017-03-09/";

  private ObjectMapper om;
  private SegmentDetector analyzer;

  @Inject public SegmentDetecterTester(SegmentDetector analyzer, ObjectMapper om) {
    this.analyzer = analyzer;
    this.om = om;
  }

  public List<TripSegment> testFromFile(File itinearyFile)
      throws Exception {

    Itinerary itinerary = om.readValue(itinearyFile, Itinerary.class);
    List<TripSegment> segments = analyzer.analyze(itinerary);
    return segments;
  }

  public void outputJson(File itinearyFile, List<TripSegment> segments)
      throws IOException {
    om.writeValue(new File(SEGMENT_ROOT + DATE + itinearyFile.getName()), segments);
  }

  public void outputCsv(File itinearyFile, List<TripSegment> segments)
      throws IOException {
    String header = "Nature, Start Date, End Date, Duration, RecommendedAccommodation, RecommendedActivity, Transfer\n";
    String format = "%s, %s, %s, %s, %s, %s, %s\n";
    FileWriter w = new FileWriter(new File(SEGMENT_ROOT + DATE + itinearyFile.getName().replace("json", "csv")));
    w.write(header);
    segments.stream().forEach(item -> {
      try {
        w.write(String.format(format,
                              getNatures(item.getNatures()),
                              item.getStartDateTime(),
                              item.getEndDateTime(),
                              item.getDurationHours(),
                              getIds(item.getAccommodations()),
                              getIds(item.getActivities()),
                              getIds(item.getTransfers())));
      }
      catch (IOException e) {
      }
    });
    w.close();
  }

  private String getNatures(Set<SegmentNature> natures) {
    StringBuilder builder = new StringBuilder();
    if (natures != null) {
      natures.stream().forEach(item -> {
        builder.append(item);
        builder.append(";");
      });
    }
    return builder.length() > 0 ? builder.substring(0, builder.length() - 1).toString() : "";
  }

  private String getIds(List<ItineraryItem> segments) {
    StringBuilder builder = new StringBuilder();
    if (segments != null) {
      segments.stream().forEach(item -> {
        builder.append(item.getUmId());
        builder.append(String.format("(%s)", item.getDuration()));
        builder.append("/");
      });
    }

    return builder.length() > 0  ? builder.substring(0, builder.length()-1).toString() : "";
  }


  public static void main(String[] args)
      throws Exception {
    Injector container = Guice.createInjector(new ItineraryAnalyzeModule());

    SegmentDetecterTester tester = container.getInstance(SegmentDetecterTester.class);

    File dir = new File(ITINERARY_ROOT + DATE);

    File[] files = dir.listFiles((direction, name) -> name.endsWith(".json"));

    for (File f : files) {
      List<TripSegment> segments = tester.testFromFile(f);
      tester.outputJson(f, segments);
      tester.outputCsv(f, segments);
    }
  }
}
