package com.umapped.itinerary.analyze.segment;

/**
 * Created by wei on 2017-03-03.
 */
public enum TransitCategory {

  // Flight, train
  ContinuousTransit(true, false, true),

  // Cruise, long train ride?
  TransitAccommodation(true, true, true),

  // car rental
  SegmentedTransit(true, true, false),

  ShortTransit(true, false, false),

  Accommodation(false, true, true),

  Activity(false, false, false),

  NonTransit(false, true, false);

  private boolean transit;
  private boolean allowActivity;
  private boolean withAccommodation;

  TransitCategory(boolean transit, boolean allowActivity, boolean withAccommodation) {
    this.transit = transit;
    this.allowActivity = allowActivity;
    this.withAccommodation = withAccommodation;
  }

  public boolean isTransit() {
    return transit;
  }

  public boolean isAllowActivity() {
    return allowActivity;
  }

  public boolean isWithAccommodation() {
    return withAccommodation;
  }
}
