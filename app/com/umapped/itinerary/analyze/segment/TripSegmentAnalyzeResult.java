package com.umapped.itinerary.analyze.segment;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.umapped.itinerary.analyze.segment.decisiontree.DecisionResult;
import com.umapped.itinerary.analyze.segment.feature.SegmentFeature;

import java.util.EnumMap;
import java.util.List;

/**
 * Created by wei on 2017-03-29.
 */
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class TripSegmentAnalyzeResult  {
  private TripSegment segment;
  private EnumMap<SegmentFeature, Object> featureValues;
  private List<DecisionResult> decisionResults;

  public TripSegment getSegment() {
    return segment;
  }

  public TripSegmentAnalyzeResult setSegment(TripSegment segment) {
    this.segment = segment;
    return this;
  }

  public EnumMap<SegmentFeature, Object> getFeatureValues() {
    return featureValues;
  }

  public TripSegmentAnalyzeResult setFeatureValues(EnumMap<SegmentFeature, Object> featureValues) {
    this.featureValues = featureValues;
    return this;
  }

  public List<DecisionResult> getDecisionResults() {
    return decisionResults;
  }

  public TripSegmentAnalyzeResult setDecisionResults(List<DecisionResult> decisionResults) {
    this.decisionResults = decisionResults;
    return this;
  }
}
