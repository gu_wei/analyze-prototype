package com.umapped.itinerary.analyze.segment.feature;

import com.umapped.itinerary.analyze.model.ItineraryItem;
import com.umapped.itinerary.analyze.model.ItineraryItemType;
import com.umapped.itinerary.analyze.model.StayPeriod;
import com.umapped.itinerary.analyze.segment.TripSegment;
import com.umapped.itinerary.analyze.model.Location;
import com.umapped.itinerary.analyze.segment.AnalyzerContext;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;

import java.time.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by wei on 2017-03-09.
 */
public class SegmentFeatureDetectorCollection {
  // value range from 0 to 8.
  public static int MAX_OVERNIGHT_SPAN = 8;
  private static LocalTime CloseToMidNight = LocalTime.of(23, 59);

  public static int overnightSpan(AnalyzerContext context) {
    TripSegment current = context.getCurrent();
    if (Duration.between(current.getStartDateTime(), current.getEndDateTime()).toHours() >= 24) {
      return MAX_OVERNIGHT_SPAN;
    }


    LocalDateTime midnight = LocalDateTime.of(current.getEndDateTime().toLocalDate(), LocalTime.MIDNIGHT);

    if (midnight.isBefore(current.getEndDateTime().toLocalDateTime()) && midnight.isAfter(current.getStartDateTime()
                                                                                                 .toLocalDateTime())) {

      long beforeMidNight = Math.min(2,
                                     Duration.between(current.getStartDateTime().toLocalTime(), CloseToMidNight)
                                             .toHours());
      long afterMidNight = Math.min(6, Duration.between(LocalTime.MIDNIGHT,

                                                        current.getEndDateTime().toLocalTime()).toHours());

      return (int) (beforeMidNight + afterMidNight);
    }
    else {
      return 0;
    }
  }

  public static List<StayPeriod> daysWithoutAccommodation(TripSegment segment) {
    // startDate, endDate counts only overnight stay dates
    LocalDate startDate = segment.getStartDateTime().toLocalDate();
    LocalDate endDate = segment.getEndDateTime().toLocalDate().minusDays(1);

    StayPeriod existingBooking = getAccomondationStayPeriod(segment);
    List<StayPeriod> periods = new ArrayList<>();
    if (existingBooking == null) {
      periods.add(new StayPeriod(startDate, endDate));
    } else {
      if (existingBooking.getStart().isAfter(startDate)) {
        periods.add(new StayPeriod(startDate, existingBooking.getStart().minusDays(1)));
      }
      if (existingBooking.getEnd().isBefore(endDate)) {
        periods.add(new StayPeriod(existingBooking.getEnd().plusDays(1), endDate));
      }
    }
    return periods;
  }

  public static List<StayPeriod> daysWithoutActivites(TripSegment segment) {
    // startDate, endDate counts only overnight stay dates
    LocalDate startDate = segment.getStartDateTime().toLocalDate();

    // TODO: After 6 pm, starting from next date
    if (segment.getStartDateTime().toLocalTime().getHour()>= 12) {
      startDate = startDate.plusDays(1);
    }
    LocalDate endDate = segment.getEndDateTime().toLocalDate();

    // TODO: Ending before 12, end previous date
    if (segment.getEndDateTime().toLocalTime().getHour() <= 12) {
      endDate = endDate.minusDays(1);
    }

    List<StayPeriod> results = new ArrayList<>();

    LocalDate current = startDate;
    if (!CollectionUtils.isEmpty(segment.getActivities())) {
      for (ItineraryItem item : segment.getActivities()) {
        LocalDate activityStartDate = item.getStartDateTime().toLocalDate();
        LocalDate activityEndDate = item.getEndDateTime() != null ? item.getEndDateTime().toLocalDate() : null;
        // No end date time for activity, set to the segment end  for now
        if (activityEndDate == null) {
          activityEndDate = endDate;
        }
        if (activityStartDate.isAfter(current)) {
          results.add(new StayPeriod(current, activityStartDate.minusDays(1)));
        }

        current = activityEndDate.plusDays(1);
      }
    }
    if (!current.isAfter(endDate)) {
      results.add(new StayPeriod(current, endDate));
    }
    return results;
  }

  /**
   * Get the Accomondation Stay Period (counting the overnight stays essentially, i.e. not include the checkout date)
   *
   * @param segment
   *
   * @return
   */
  public static StayPeriod getAccomondationStayPeriod(TripSegment segment) {
    if (segment.getAccommodationCount() > 0) {
      // TODO:  only one accomodation per segment for now, it would be splitted if more than one accommodation
      //        But needs to think about Cruise
      ItineraryItem accommodation = segment.getAccommodations().get(0);
      return new StayPeriod(accommodation.getStartDateTime().toLocalDate(),
                            accommodation.getEndDateTime().toLocalDate().minusDays(1));
    } else {
      return null;
    }
  }

  public static boolean accomodationCancellable(AnalyzerContext context) {
    return false;

  }

  public static boolean hasCruiseStops(TripSegment segment) {
    List<ItineraryItem> items = segment.getCruiseStops();
    return (!CollectionUtils.isEmpty(items));
  }

  public static List<Pair<ZonedDateTime, Location>> getLocationSequence(AnalyzerContext context) {
    List<Pair<ZonedDateTime, Location>> sequence = new ArrayList<>();

    TripSegment current = context.getCurrent();
    sequence.add(new ImmutablePair<>(current.getStartDateTime(), current.getStartLocation()));

    List<ItineraryItem> hotels = current.getAccommodations();
    if (hotels != null) {
      for (ItineraryItem h : hotels) {
        sequence.add(new ImmutablePair<>(h.getStartDateTime(), h.getStartLocation()));
      }
    }
    // TODO: consider activities
    sequence.add(new ImmutablePair<>(current.getEndDateTime(), current.getEndLocation()));
    return sequence;
  }

  public static void main(String[] args) {
    System.out.println(Duration.between(LocalTime.of(15, 0), LocalTime.MIDNIGHT).toHours());
  }
}
