package com.umapped.itinerary.analyze.segment.feature;

/**
 * Created by wei on 2017-03-10.
 */
public enum SegmentFeature {
  // If the segment is over 24 hours
  DurationHours,
  // if the segment is shorter than 24 hours, does it span over midnight
  OverNightHours,
  AccommodationStay,
  DatesWithoutAccommodation,
  HasCancellableAccomodation,
  LocationSequence,
  HasTransfers,
  HasAccommondations,
  HasActivities,
  DatesWihoutActivities,
  HasCruiseStops
}
