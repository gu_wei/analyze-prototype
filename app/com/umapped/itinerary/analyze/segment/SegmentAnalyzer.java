package com.umapped.itinerary.analyze.segment;

/**
 * Created by wei on 2017-03-08.
 */
public interface SegmentAnalyzer {
  SegmentAnalyzeResult analyze(TripSegment segement, TripSegment previous, TripSegment next);
}
