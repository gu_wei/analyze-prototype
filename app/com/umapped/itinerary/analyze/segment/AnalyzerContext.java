package com.umapped.itinerary.analyze.segment;

import com.google.inject.Inject;
import com.umapped.itinerary.analyze.model.Location;
import com.umapped.itinerary.analyze.model.StayPeriod;
import com.umapped.itinerary.analyze.segment.feature.SegmentFeatureDetector;
import com.umapped.itinerary.analyze.segment.feature.SegmentFeatureDetectorRegistry;
import com.umapped.itinerary.analyze.segment.feature.SegmentFeature;
import org.apache.commons.lang3.tuple.Pair;

import java.time.LocalDate;
import java.time.ZonedDateTime;
import java.util.*;

/**
 * Created by wei on 2017-03-08.
 */
public class AnalyzerContext {
  private TripSegment current;
  private TripSegment previous;
  private TripSegment next;
  private EnumMap<SegmentFeature, Object> featureValues;
  private SegmentFeatureDetectorRegistry registry;

  @Inject
  public AnalyzerContext(SegmentFeatureDetectorRegistry registry) {
    this.registry = registry;
    featureValues = new EnumMap<SegmentFeature, Object>(SegmentFeature.class);
  }


  public void setup(TripSegment current, TripSegment previous, TripSegment next) {
    this.current = current;
    this.previous = previous;
    this.next = next;

  }

  public TripSegment getCurrent() {
    return current;
  }

  public TripSegment getPrevious() {
    return previous;
  }

  public TripSegment getNext() {
    return next;
  }

  public Object getFeature(SegmentFeature feature) {
    Object value = featureValues.get(feature);

    if (value == null) {
      Optional<SegmentFeatureDetector> detector = registry.getDetector(feature);
      if (detector.isPresent()) {
        value = detector.get().detect(this);
        // cache it
        featureValues.put(feature, value);
      }
    }

    return value;
  }

  public EnumMap<SegmentFeature, Object> getFeatureValues() {
    return featureValues;
  }

  public List<Location> getPossibleLocation(StayPeriod period) {
    List<Pair<ZonedDateTime, Location>> locations = (List<Pair<ZonedDateTime, Location>>) getFeature(SegmentFeature.LocationSequence);

    int starting = 0;
    for (; starting< locations.size(); starting++) {
      LocalDate locStartDate = locations.get(starting).getLeft().toLocalDate();

      if (locStartDate.isAfter(period.getStart())) {
        break;
      }
    }

    // get the one right before start date
    starting--;
    if (starting <0) {
      starting = 0;
    }
    int ending = starting;
    for (; ending< locations.size(); ending++) {
      LocalDate locStartDate = locations.get(ending).getLeft().toLocalDate();

      if (locStartDate.isAfter(period.getEnd())) {
        break;
      }
    }
    if (ending >= locations.size()) {
      ending = locations.size()-1;
    }

    List<Location> result = new ArrayList<>();
    for (int i = starting; i <= ending; i++) {
      result.add(locations.get(i).getRight());
    }
    return result;
  }

}
