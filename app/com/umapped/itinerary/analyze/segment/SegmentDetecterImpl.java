package com.umapped.itinerary.analyze.segment;

import com.umapped.itinerary.analyze.model.Itinerary;
import com.umapped.itinerary.analyze.model.ItineraryItem;
import com.umapped.itinerary.analyze.model.ItineraryItemType;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;

import java.time.Period;
import java.util.List;
import java.util.stream.Collectors;

import static com.umapped.itinerary.analyze.segment.TransitCategory.*;

/**
 * Created by wei on 2017-03-01.
 */
public class SegmentDetecterImpl
    implements SegmentDetector {
  @Override public List<TripSegment> analyze(Itinerary itinerary) {
    TripSegmentBuilder builder = new TripSegmentBuilder();

    List<Pair<TransitCategory, ItineraryItem>> itemList = itinerary.getItems()
                                                                   .stream()
                                                                   .map(SegmentDetecterImpl::mapTransitCategory)
                                                                   .collect(Collectors.toList());

    builder.startTransit();
    itemList.stream()
            .filter(kv -> kv.getLeft() == ContinuousTransit || kv.getLeft() == SegmentedTransit || kv.getLeft() == TransitAccommodation)
            .sorted((kv1, kv2) -> kv1.getRight().compareByStartTime(kv2.getRight()))
            .forEach(kv -> {
              builder.addTransitSegment(kv.getLeft(), kv.getRight());
            });

    builder.endTransit();

    itemList.stream()
            .filter(kv -> kv.getLeft() != ContinuousTransit && kv.getLeft() != SegmentedTransit)
            .sorted((kv1, kv2) -> kv1.getRight().compareByStartTime(kv2.getRight()))
            .forEach(kv -> {
              if (kv.getLeft().isWithAccommodation()) {
                builder.addAccommodationItem(kv.getRight());
              }
              else {
                builder.addActivityItem(kv.getRight());
              }
            });
    return builder.getResult();
  }

  static public Pair<TransitCategory, ItineraryItem> mapTransitCategory(ItineraryItem item) {
    Pair<TransitCategory, ItineraryItem> result = null;

    switch (item.getType()) {
      case Flight:
        result = new ImmutablePair<>(ContinuousTransit, item);
        break;
      case Cruise:
        result = new ImmutablePair<>(TransitAccommodation, item);
        break;
      case CruiseStop:
        result = new ImmutablePair<>(Activity, item);
        break;
      case Accommodation:
        result = new ImmutablePair<>(Accommodation, item);
        break;
      case Activity:
        result = new ImmutablePair<>(Activity, item);
        break;
      case LongTransfer: // TODO: need to further breakdown based on the type, i.e rail vs car rental
        if (item.getStartDateTime()!=null && item.getEndDateTime() != null) {
        // TODO: assumption: longer than 1 day, it is segmented transit
          if (Period.between(item.getStartDateTime().toLocalDate(), item.getEndDateTime().toLocalDate()).getDays() > 1) {
            result = new ImmutablePair<>(SegmentedTransit, item);
          }
        }
        if (result == null) {
          result = new ImmutablePair<>(ContinuousTransit, item);
        }
        break;
      case ShortTransfer:
        // TODO: not sure if this correct yet
        result = new ImmutablePair<>(ShortTransit, item);
        break;
    }
    return result;
  }

  static public Boolean isTransitItinerary(ItineraryItem item) {
    ItineraryItemType t = item.getType();
    return t.equals(ItineraryItemType.Flight) || t.equals(ItineraryItemType.Cruise) || t.equals(ItineraryItemType.LongTransfer);
  }
}
