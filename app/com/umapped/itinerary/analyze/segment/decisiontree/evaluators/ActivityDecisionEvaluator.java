package com.umapped.itinerary.analyze.segment.decisiontree.evaluators;

import com.umapped.itinerary.analyze.model.ItineraryItemType;
import com.umapped.itinerary.analyze.model.StayPeriod;
import com.umapped.itinerary.analyze.segment.decisiontree.Evaluator;
import com.umapped.itinerary.analyze.segment.AnalyzerContext;
import com.umapped.itinerary.analyze.segment.decisiontree.DecisionResult;
import com.umapped.itinerary.analyze.segment.feature.SegmentFeature;
import org.apache.commons.collections.CollectionUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by wei on 2017-03-12.
 */
public class ActivityDecisionEvaluator implements Evaluator {
  @Override public Object evaluate(AnalyzerContext context) {
    List<StayPeriod> daysWithoutActivities = (List<StayPeriod>)context.getFeature(SegmentFeature.DatesWihoutActivities);

    // TODO: simplify it with the starting location for now
    List<DecisionResult> results = new ArrayList<>();

    if (!CollectionUtils.isEmpty(daysWithoutActivities)) {
      for (StayPeriod p : daysWithoutActivities) {
        results.add(new DecisionResult(ItineraryItemType.Activity, context.getPossibleLocation(p), p.getStart(), p.getEnd()));
      }
    }
    return results;
  }
}
