package com.umapped.itinerary.analyze.segment.decisiontree.evaluators;

import com.umapped.itinerary.analyze.model.ItineraryItem;
import com.umapped.itinerary.analyze.model.ItineraryItemType;
import com.umapped.itinerary.analyze.model.StayPeriod;
import com.umapped.itinerary.analyze.segment.AnalyzerContext;
import com.umapped.itinerary.analyze.segment.decisiontree.DecisionResult;
import com.umapped.itinerary.analyze.segment.decisiontree.Evaluator;
import com.umapped.itinerary.analyze.segment.feature.SegmentFeature;
import org.apache.commons.collections.CollectionUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by wei on 2017-04-04.
 */
public class CruiseActivityDecisionEvaluator implements Evaluator {
  @Override public Object evaluate(AnalyzerContext context) {

    List<StayPeriod> daysWithoutActivities = (List<StayPeriod>)context.getFeature(SegmentFeature.DatesWihoutActivities);

    // TODO: simplify it with the starting location for now
    List<DecisionResult> results = new ArrayList<>();

    List<ItineraryItem> items = context.getCurrent().getCruiseStops();
    if (!CollectionUtils.isEmpty(items)) {
      for (ItineraryItem i : items) {
        if (ItineraryItemType.CruiseStop.equals(i.getType())) {
          if (i.getStartLocation().getGeoLocation() != null) {
            results.add(new DecisionResult(ItineraryItemType.Activity, i.getStartLocation(), i.getStartDateTime().toLocalDate()));
          }
        }
      }
    }
    return results;
  }
}
