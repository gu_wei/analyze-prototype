package com.umapped.itinerary.analyze.segment.decisiontree;

import com.umapped.itinerary.analyze.segment.AnalyzerContext;

import java.util.List;

/**
 * Created by wei on 2017-03-10.
 */
abstract public class AbstractDecisionTree implements DecisionTree {

  protected DecisionNode root;

  abstract protected DecisionNode initTree();

  public AbstractDecisionTree() {
    root = initTree();
  }

  protected DecisionNode getRoot() {
    return root;
  }

  @Override
  public List<DecisionResult> decide(AnalyzerContext context) {
    DecisionNode next = getRoot();
    while (!next.isLeaf()) {
      next = next.next(context);
    }
    List<DecisionResult> result = (List<DecisionResult>) next.getEvaluateResult(context);
    return result;
  }
}
