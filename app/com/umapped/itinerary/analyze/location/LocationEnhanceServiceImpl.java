package com.umapped.itinerary.analyze.location;

import com.umapped.itinerary.analyze.db.model.publisher.PoiLookupService;
import com.umapped.itinerary.analyze.location.google.GoogleLocationLookupSevice;
import com.umapped.itinerary.analyze.model.Location;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.time.ZoneId;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by wei on 2017-03-29.
 */
@Singleton
public class LocationEnhanceServiceImpl implements LocationEnhanceService {
  private Map<String, Location> cache;

  private TimeZoneLookupService timeZoneLookupService;
  private LocationLookupService poiLookupService;
  private GoogleLocationLookupSevice googleLocationLookupSevice;

  @Inject
  public LocationEnhanceServiceImpl(TimeZoneLookupService timeZoneLookupService) {
    this.timeZoneLookupService = timeZoneLookupService;
    poiLookupService = new PoiLookupService();
    googleLocationLookupSevice = new GoogleLocationLookupSevice();
    cache = new HashMap<>();
  }

  @Override public Location enhanceLocation(Location location, boolean important) {
    if (location == null) {
      return null;
    }
    Location enhanced = lookupFromCache(location);
    if (enhanced == null) {
      if (location.getGeoLocation() == null) {
        enhanced = lookupGeoLocation(location, important);
      } else {
        enhanced = location;
      }

      if (location.getTimezone() == null) {
        ZoneId zoneId = lookupTimeZone(location.getGeoLocation());
        enhanced.setTimezone(zoneId);
      }
      // if it is enhanced, put it into cache
      if (enhanced.getGeoLocation() != null) {
        cache.put(getLocationKey(location), enhanced);
      }
    }
    return enhanced;
  }

  private Location lookupGeoLocation(Location location, boolean important) {
    Location enahnced = null;
    if (location.getUmPoidId() != null) {
      enahnced = poiLookupService.lookup(location);
    }
    // try google
    if ((enahnced == null || enahnced.getGeoLocation() == null) && important) {
      enahnced = googleLocationLookupSevice.lookup(location);
    }

    if (enahnced == null) {
      enahnced = location;
    }
    // still not found
    return enahnced;
  }

  private ZoneId lookupTimeZone(Location.GeoLocation geoLocation) {
    if (geoLocation == null) {
      return null;
    }
    return timeZoneLookupService.lookup(geoLocation);
  }

  private Location lookupFromCache(Location location) {
      return cache.get(getLocationKey(location));
  }

  private String getLocationKey(Location location) {
    if (location.getUmPoidId() != null) {
      return location.getUmPoidId();
    } else {
      return location.getDescription();
    }
  }
}
