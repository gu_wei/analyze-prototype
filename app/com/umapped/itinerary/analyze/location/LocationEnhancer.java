package com.umapped.itinerary.analyze.location;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.inject.Guice;
import com.google.inject.Inject;
import com.google.inject.Injector;
import com.umapped.itinerary.analyze.ItineraryAnalyzeModule;
import com.umapped.itinerary.analyze.WebServiceModule;
import com.umapped.itinerary.analyze.model.Itinerary;
import com.umapped.itinerary.analyze.model.ItineraryItem;
import com.umapped.itinerary.analyze.model.Location;

import java.io.File;
import java.util.concurrent.CompletableFuture;

/**
 * Created by wei on 2017-03-22.
 */
public class LocationEnhancer {

  private TimeZoneLookupService timeZoneLookupService;


  @Inject public LocationEnhancer(TimeZoneLookupService timeZoneLookupService) {
    this.timeZoneLookupService = timeZoneLookupService;
  }

  private CompletableFuture asyncGetLocation(CompletableFuture previous, Location location) {
    CompletableFuture current = null;
    if (location != null && location.getGeoLocation() != null) {
      current = CompletableFuture.supplyAsync(() -> timeZoneLookupService.lookup(location.getGeoLocation()))
                                 .whenComplete((zoneId, throwable) -> {
                                   location.setTimezone(zoneId);
                                 })
                                 .toCompletableFuture();

    }
    if (previous != null) {
      if (current != null) {
        return previous.allOf(previous, current);
      }
      else {
        return previous;
      }
    }
    else {
      return current;
    }
  }

  private void printLocation(Location location) {
    System.out.println(location.getDescription() + ":" + location.getGeoLocation() + ":" + location.getTimezone());
  }

  private void printTimeZone(Itinerary itinerary) {
    for (ItineraryItem item : itinerary.getItems()) {
      printLocation(item.getStartLocation());
      printLocation(item.getEndLocation());
    }
  }


  public void getAsyncLocation(Itinerary itinerary) {
    final long t1 = System.currentTimeMillis();
    CompletableFuture allTimeZones = null;
    for (ItineraryItem item : itinerary.getItems()) {
      allTimeZones = asyncGetLocation(allTimeZones, item.getStartLocation());
      allTimeZones = asyncGetLocation(allTimeZones, item.getEndLocation());
    }

    if (allTimeZones == null) {
      printTimeZone(itinerary);
      System.out.println("Done");
    }
    else {
      allTimeZones.whenComplete(((o, throwable) -> {
        System.out.println("Done : " + (System.currentTimeMillis() - t1));
        printTimeZone(itinerary);
      }));
      allTimeZones.join();
    }
  }

  public void getSyncLocation(Itinerary itinerary) {
    final long t1 = System.currentTimeMillis();
    for (ItineraryItem item : itinerary.getItems()) {
      if (item.getStartLocation() != null && item.getStartLocation().getGeoLocation() != null) {
        item.getStartLocation().setTimezone(timeZoneLookupService.lookup(item.getStartLocation().getGeoLocation()));
      }

      if (item.getEndLocation() != null && item.getEndLocation().getGeoLocation() != null) {
        item.getEndLocation().setTimezone(timeZoneLookupService.lookup(item.getEndLocation().getGeoLocation()));
      }
    }
    System.out.println("Sync done: " + (System.currentTimeMillis()-t1));
    printTimeZone(itinerary);

  }

  public static void main(String[] args)
      throws Exception {
    Injector container = Guice.createInjector(new ItineraryAnalyzeModule(), new WebServiceModule());

    ObjectMapper om = container.getInstance(ObjectMapper.class);
    Itinerary itinerary = om.readValue(new File(
        "/Users/wei/DevResources/data/trips/itinerary/2017-03-09/1320929702090001363.json"), Itinerary.class);
    LocationEnhancer enhancer = container.getInstance(LocationEnhancer.class);
    enhancer.getAsyncLocation(itinerary);
    enhancer.getSyncLocation(itinerary);
  }
}
