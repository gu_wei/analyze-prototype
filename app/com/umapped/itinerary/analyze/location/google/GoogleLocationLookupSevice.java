package com.umapped.itinerary.analyze.location.google;

import com.google.maps.GeoApiContext;
import com.google.maps.GeocodingApi;
import com.google.maps.GeocodingApiRequest;
import com.google.maps.model.AddressComponent;
import com.google.maps.model.AddressComponentType;
import com.google.maps.model.ComponentFilter;
import com.google.maps.model.GeocodingResult;
import com.umapped.itinerary.analyze.location.LocationLookupService;
import com.umapped.itinerary.analyze.model.Location;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by wei on 2017-03-22.
 */
public class GoogleLocationLookupSevice
    implements LocationLookupService {

  private static final Logger LOG = LoggerFactory.getLogger(GoogleLocationLookupSevice.class);

  private String getSearchString(final Location location) {
    // favor address first
    Location.Address address = location.getAddress();
    StringBuilder builder = new StringBuilder();
    if (address != null) {

      if (!StringUtils.isEmpty(address.getStreetAddress())) {
        builder.append(StringUtils.trimToEmpty(address.getStreetAddress()));
        builder.append(",");
      }
      if (!StringUtils.isEmpty(address.getLocality())) {
        builder.append(StringUtils.trimToEmpty(address.getLocality()));
        builder.append(",");
      }
      if (!StringUtils.isEmpty(address.getRegion())) {
        builder.append(StringUtils.trimToEmpty(address.getRegion()));
        builder.append(",");
      }
      if (!StringUtils.isEmpty(address.getCountry())) {
        builder.append(StringUtils.trimToEmpty(address.getCountry()));
      }
    }
    if (builder.length() > 10) {
      return builder.toString();
    } else {
      return location.getDescription() + builder.toString();
    }
  }
  @Override public Location lookup(final Location location) {
    String searchString = getSearchString(location);
    if (StringUtils.isEmpty(searchString)) {
      return location;
    }
    GeoApiContext context = GoogleAPISupport.getGeoApiContext();
    GeocodingApiRequest request = GeocodingApi.geocode(context, searchString);

    try {
      GeocodingResult[] gResults = request.await();
      Location result = extract(gResults, false);
      // no return
      // let's see narrow to region will help
      if (result == null) {
        request = GeocodingApi.geocode(context, searchString);
        if (StringUtils.isNotEmpty(location.getAddress().getA2CountryCode())) {
          request.components(ComponentFilter.country(location.getAddress().getA2CountryCode()));
          result = extract(request.await(), true);
        } else {
          result = extract(gResults, true);
        }
        if (result != null) {
          result.setDescription(location.getDescription());
        }
      }
      return result;
    }
    catch (Exception e) {
      throw new GoogleAPIException(e);
    }
  }

  public Location extract(GeocodingResult[] results, boolean takeFirst) {
    if (results.length > 1 && !takeFirst) {
      LOG.error("Multiple results returned");
      return null;
    } else if (results.length == 0) {
      LOG.error("No results returned");
      return null;
    }
    Location location = new Location();

    GeocodingResult r = results[0];
    location.addPlaceId("google", r.placeId);
    location.setGeoLocation(new Location.GeoLocation(r.geometry.location.lat, r.geometry.location.lng));
    updateLocationAddress(location, r.addressComponents);
    return location;
  }

  public void updateLocationAddress(Location location, AddressComponent[] addresses) {
    for (AddressComponent a : addresses) {
      for (AddressComponentType t : a.types) {
        switch (t) {
          case COUNTRY:
            location.getAddress().setA2CountryCode(a.shortName);
            location.getAddress().setCountry(a.longName);
            break;
          case LOCALITY:
            location.getAddress().setLocality(a.longName);
            break;
          case ADMINISTRATIVE_AREA_LEVEL_1:
            location.getAddress().setRegion(a.longName);
            break;
          case ADMINISTRATIVE_AREA_LEVEL_2:
            if (location.getAddress().getLocality() == null) {
              location.getAddress().setLocality(a.longName);
            }
            break;
          case STREET_ADDRESS:
            location.getAddress().setStreetAddress(a.longName);
            break;
        }
      }
    }
  }

  public static void main(String[] args) throws Exception {
    GoogleLocationLookupSevice service = new GoogleLocationLookupSevice();
    Location loc = new Location();
    loc.setDescription("YX ATLANTIS ROYAL TOWER");
    Location result = service.lookup(loc);
    LOG.info(result.toString());
    //loc.getAddress().setA2CountryCode("PE");
    result = service.lookup(loc);
    LOG.info(result.toString());
  }
}
