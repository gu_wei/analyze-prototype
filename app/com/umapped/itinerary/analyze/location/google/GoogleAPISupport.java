package com.umapped.itinerary.analyze.location.google;

import com.google.maps.GeoApiContext;

/**
 * Created by wei on 2017-03-22.
 */
public class GoogleAPISupport {
  private static String API_KEY = "AIzaSyA7an8HI6xXMqFq0Yofm2UjT3ZFtbmni2k";

  public static GeoApiContext getGeoApiContext() {
    GeoApiContext context = new GeoApiContext();
    context.setApiKey(API_KEY);
    return context;
  }
}
