package com.umapped.itinerary.analyze.location;

import com.umapped.itinerary.analyze.model.Location;

/**
 * Created by wei on 2017-03-22.
 */
public interface LocationLookupService {
  Location lookup(Location location);
}
