package com.umapped.itinerary.analyze.location;

import com.umapped.itinerary.analyze.model.Location;

import java.time.ZoneId;

/**
 * Created by wei on 2017-03-22.
 */
public interface TimeZoneLookupService {
  ZoneId lookup(Location.GeoLocation location);
}
