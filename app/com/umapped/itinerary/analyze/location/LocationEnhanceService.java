package com.umapped.itinerary.analyze.location;

import com.umapped.itinerary.analyze.model.Location;

/**
 * Created by wei on 2017-03-29.
 */
public interface LocationEnhanceService {
  Location enhanceLocation(Location location, boolean important);
}
