package com.umapped.itinerary.analyze.location.geonames;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by wei on 2017-03-22.
 */
public class GeoNamesAPIRequest {
  private Map<String, String> params;

  public GeoNamesAPIRequest() {
    params = new HashMap<>();
  }

  public void addParam(String name, String value) {
    params.put(name, value);
  }

  public String buildQueryString()
      throws UnsupportedEncodingException {
    StringBuilder builder = new StringBuilder();
    for (String paramName : params.keySet()) {
      append(builder, paramName, params.get(paramName));
      builder.append("&");
    }
    append(builder, GeoNamesWebServiceRequestHandler.PARAM_USER_ID, GeoNamesWebServiceRequestHandler.UMAPPED_USER_NAME);
    return builder.toString();
  }

  private void append(StringBuilder builder, String name, String value)
      throws UnsupportedEncodingException {
    builder.append(name);
    builder.append("=");
    builder.append(URLEncoder.encode(value, "UTF-8"));
  }
}
