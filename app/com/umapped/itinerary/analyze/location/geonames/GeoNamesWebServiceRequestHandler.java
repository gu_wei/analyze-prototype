package com.umapped.itinerary.analyze.location.geonames;

import akka.actor.ActorSystem;
import akka.stream.ActorMaterializer;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by wei on 2017-03-22.
 */
@Singleton public class GeoNamesWebServiceRequestHandler {
  public static String SERVER_URL = "http://api.geonames.org/";

  public static String TIME_ZONE_API_END_POINT = "timezoneJSON";

  public static String PARAM_USER_ID = "username";

  public static String PARAM_LATITUDE = "lat";

  public static String PARAM_LONGITUDE = "lng";

  public static String UMAPPED_USER_NAME = "umapped_1";

  private ObjectMapper objectMapper;

  @Inject public GeoNamesWebServiceRequestHandler(ObjectMapper objectMapper) {
    this.objectMapper = objectMapper;
  }

  public Map call(String endpoint, String queryString) {
    String url = SERVER_URL + endpoint + "?" + queryString;

    HttpGet request = new HttpGet(url);

    try (CloseableHttpClient client = HttpClients.createDefault();
         CloseableHttpResponse response = client.execute(request)) {
      HttpEntity entity = response.getEntity();
      Map<String, Object> result = objectMapper.readValue(entity.getContent(), HashMap.class);
      return result;
    }
    catch (IOException e) {
      throw new GeoNamesServiceException(e);
    }
  }

}
