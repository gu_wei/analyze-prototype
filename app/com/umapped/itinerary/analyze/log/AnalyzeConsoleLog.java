package com.umapped.itinerary.analyze.log;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mapped.publisher.parse.schemaorg.ReservationPackage;
import com.umapped.itinerary.analyze.ItineraryAnalyzeResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.io.IOException;
import java.io.StringWriter;
import java.util.Map;

/**
 * Created by wei on 2017-03-29.
 */
@Singleton
public class AnalyzeConsoleLog implements ItineraryAnalyzeServiceLog {
  private static final Logger LOG = LoggerFactory.getLogger(AnalyzeConsoleLog.class);

  private ObjectMapper objectMapper;

  @Inject
  public AnalyzeConsoleLog(ObjectMapper objectMapper) {
    this.objectMapper = objectMapper;
  }

  @Override
  public void log(ReservationPackage reservationPackage, ItineraryAnalyzeResult result, Map<String, Long> performance) {
    if (LOG.isDebugEnabled()) {
      try {
        StringWriter rpWriter = new StringWriter();
        objectMapper.writeValue(rpWriter, reservationPackage);

        StringWriter resultWriter = new StringWriter();
        objectMapper.writeValue(resultWriter, result);

        StringWriter performanceWriter = new StringWriter();
        objectMapper.writeValue(performanceWriter, performance);

        LOG.debug("Analyze Input: " + rpWriter);
        LOG.debug("Analyze Result:" + resultWriter);
        LOG.debug("Analyze Performance:" + performanceWriter);

      } catch(IOException e){
        LOG.error("Unexpected error", e);
      }
    }
  }
}
