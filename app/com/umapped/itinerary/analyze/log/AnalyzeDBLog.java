package com.umapped.itinerary.analyze.log;

import com.avaje.ebean.Ebean;
import com.avaje.ebean.EbeanServerFactory;
import com.avaje.ebean.config.ServerConfig;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mapped.publisher.parse.schemaorg.ReservationPackage;
import com.umapped.itinerary.analyze.ItineraryAnalyzeResult;
import com.umapped.itinerary.analyze.db.model.ItineraryAnalyzeResultModel;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.util.Map;

/**
 * Created by wei on 2017-03-30.
 */
@Singleton
public class AnalyzeDBLog implements ItineraryAnalyzeServiceLog {
  private ObjectMapper objectMapper;

  @Inject
  public AnalyzeDBLog(ObjectMapper objectMapper) {
    this.objectMapper = objectMapper;
    //initDB();
  }

  private void initDB() {
    ServerConfig config = new ServerConfig();
    config.setName("db");

    config.loadFromProperties();

    // set as default and register so that Model can be
    // used if desired for save() and update() etc
    config.setDefaultServer(true);
    config.setRegister(true);

    EbeanServerFactory.create(config);
  }

  @Override
  public void log(ReservationPackage reservationPackage, ItineraryAnalyzeResult result, Map<String, Long> performance) {
    ItineraryAnalyzeResultModel model = new ItineraryAnalyzeResultModel();

    model.setAnalyzeId(System.currentTimeMillis());
    model.setItineraryId(result.getItineraryId());
    model.setReservationPackage(objectMapper.valueToTree(reservationPackage));
    model.setItineraryAnalyzeResult(objectMapper.valueToTree(result));
    model.setPerformance(objectMapper.valueToTree(performance));
    Ebean.save(model);
  }
}
