package com.umapped.itinerary.analyze;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.inject.AbstractModule;
import com.umapped.itinerary.analyze.location.LocationEnhanceService;
import com.umapped.itinerary.analyze.location.LocationEnhanceServiceImpl;
import com.umapped.itinerary.analyze.location.LocationLookupService;
import com.umapped.itinerary.analyze.location.TimeZoneLookupService;
import com.umapped.itinerary.analyze.location.geonames.GeoNamesTimeZoneLookupService;
import com.umapped.itinerary.analyze.log.AnalyzeConsoleLog;
import com.umapped.itinerary.analyze.log.AnalyzeDBLog;
import com.umapped.itinerary.analyze.log.ItineraryAnalyzeServiceLog;
import com.umapped.itinerary.analyze.model.json.ObjectMapperProvider;
import com.umapped.itinerary.analyze.recommendation.RecommendationService;
import com.umapped.itinerary.analyze.recommendation.RecommendationServiceImpl;
import com.umapped.itinerary.analyze.segment.SegmentDetecterImpl;
import com.umapped.itinerary.analyze.segment.SegmentDetector;
import com.umapped.itinerary.analyze.segment.feature.DefaultFeatureDetectorRegistory;
import com.umapped.itinerary.analyze.segment.feature.SegmentFeatureDetectorRegistry;
import com.umapped.itinerary.analyze.supplier.ProviderRepository;
import com.umapped.itinerary.analyze.supplier.ProviderRepositoryImpl;
import com.umapped.itinerary.analyze.supplier.shoretrip.PortLookup;
import com.umapped.itinerary.analyze.supplier.shoretrip.ShoreTripProvider;
import com.umapped.itinerary.analyze.supplier.shoretrip.ShoreTripWebService;
import com.umapped.itinerary.analyze.supplier.travelbound.TraveBoundProvider;
import com.umapped.itinerary.analyze.supplier.travelbound.TravelBoundWebService;
import com.umapped.itinerary.analyze.supplier.wcities.WCitiesCityLookup;
import com.umapped.itinerary.analyze.supplier.wcities.WCitiesProivder;
import com.umapped.itinerary.analyze.supplier.wcities.WCitiesWebService;

/**
 * Created by wei on 2017-02-16.
 */
public class ItineraryAnalyzeModule
    extends AbstractModule {
    @Override
    protected void configure() {
        bind(ObjectMapper.class).toProvider(ObjectMapperProvider.class);
        bind(SegmentDetector.class).to(SegmentDetecterImpl.class);
        bind(SegmentFeatureDetectorRegistry.class).to(DefaultFeatureDetectorRegistory.class);
        bind(ItineraryAnalyzer.class).toProvider(ItineraryAnalyzerProvider.class);
        bind(ItineraryAnalyzeServiceLog.class).to(AnalyzeConsoleLog.class);
        bind(LocationEnhanceService.class).to(LocationEnhanceServiceImpl.class);
        bind(TimeZoneLookupService.class).to(GeoNamesTimeZoneLookupService.class);
        bind(ItineraryLocationEnhancer.class);
        bind(WCitiesWebService.class);
        bind(WCitiesCityLookup.class);
        bind(PortLookup.class);
        bind(ShoreTripWebService.class);
        bind(TravelBoundWebService.class);
        bind(ItineraryAnalyzeService.class).to(ItineraryAnalyzeServiceImpl.class);
        bind(RecommendationService.class).to(RecommendationServiceImpl.class);
        bind(WCitiesProivder.class);
        bind(ShoreTripProvider.class);
        bind(TraveBoundProvider.class);
        bind(ProviderRepository.class).to(ProviderRepositoryImpl.class);
    }
}
