package com.umapped.itinerary.analyze.tests;

import com.avaje.ebean.EbeanServerFactory;
import com.avaje.ebean.config.ServerConfig;

/**
 * Created by wei on 2017-04-05.
 */
public class EBeanServerConfig {
  public static void init() {
        ServerConfig config = new ServerConfig();
        config.setName("db");

        config.loadFromProperties();

        // set as default and register so that Model can be
        // used if desired for save() and update() etc
        config.setDefaultServer(true);
        config.setRegister(true);

        EbeanServerFactory.create(config);
  }
}
