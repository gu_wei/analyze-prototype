package com.umapped.itinerary.analyze.supplier.travelbound;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.umapped.itinerary.analyze.TripFeature;
import com.umapped.itinerary.analyze.model.Location;
import com.umapped.itinerary.analyze.model.StayPeriod;
import com.umapped.itinerary.analyze.recommendation.RecommendedItem;
import com.umapped.itinerary.analyze.segment.decisiontree.DecisionResult;
import com.umapped.itinerary.analyze.segment.feature.SegmentFeature;
import com.umapped.itinerary.analyze.supplier.AccommondationProvider;
import com.umapped.itinerary.analyze.supplier.ProviderHelper;

import java.time.LocalDate;
import java.util.Collections;
import java.util.EnumMap;
import java.util.List;

/**
 * Created by wei on 2017-04-20.
 */
@Singleton
public class TraveBoundProvider implements AccommondationProvider {
  private TravelBoundWebService travelBoundWebService;
  private ProviderHelper providerHelper;

  @Inject
  public TraveBoundProvider(TravelBoundWebService travelBoundWebService, ProviderHelper providerHelper) {
    this.travelBoundWebService = travelBoundWebService;
  }


  @Override
  public List<RecommendedItem> findAccommondations(DecisionResult decisionResult,
                                                   EnumMap<TripFeature, Object> tripFeatures,
                                                   EnumMap<SegmentFeature, Object> segmentFeatures) {
    Location location = providerHelper.findLocation(decisionResult);
    if (location == null) {
      return Collections.emptyList();
    }


    LocalDate startDate = decisionResult.getStartDate();
    LocalDate endDate = decisionResult.getEndDate();
    StayPeriod period = new StayPeriod(startDate, endDate);

    return travelBoundWebService.getRecommendedHotels(period, location);
  }
}
