package com.umapped.itinerary.analyze.supplier.wcities;

import com.avaje.ebean.Ebean;
import com.avaje.ebean.SqlQuery;
import com.avaje.ebean.SqlRow;
import com.umapped.itinerary.analyze.model.Location;

import javax.inject.Singleton;
import java.util.List;

/**
 * Created by wei on 2017-03-31.
 */
@Singleton
public class WCitiesCityLookup {

  private static final String City_Lookup = "SELECT city_id as city_id, name as name, ST_Distance(location, ST_MakePoint(:lng, :lat)) distance " +
               " FROM wcities_city WHERE ST_Distance(location, ST_MakePoint(:lng, :lat)) <= :radius * 1000 order by distance asc";


  public WCitiesCityLookup() {
  }

  public String getCityId(Location.GeoLocation location) {
    if (location == null) {
      return null;
    }
    List<SqlRow> rows = Ebean.createSqlQuery(City_Lookup)
                             .setParameter("lng", location.getLongitude())
                             .setParameter("lat", location.getLatitude())
                             .setParameter("radius", 50).findList();
    if (rows.size() > 0) {
      return rows.get(0).getString("city_id");
    } else {
      return null;
    }
  }
}
