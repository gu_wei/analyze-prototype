
package com.umapped.itinerary.analyze.supplier.wcities.response;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "id",
    "distance",
    "category",
    "name",
    "short_desc",
    "long_desc",
    "booking_url",
    "address",
    "neighborhood",
    "phone",
    "email",
    "url",
    "image_attribution",
    "nearest_train",
    "nearest_bus",
    "credit_card",
    "eds_choice",
    "imagefile",
    "fax",
    "disabled_access"
})
public class Details {

    @JsonProperty("id")
    private String id;
    @JsonProperty("distance")
    private String distance;
    @JsonProperty("category")
    private List<Category> category;
    @JsonProperty("name")
    private String name;
    @JsonProperty("short_desc")
    private String shortDesc;
    @JsonProperty("long_desc")
    private String longDesc;
    @JsonProperty("booking_url")
    private List<String> bookingUrl;
    @JsonProperty("address")
    private Address address;
    @JsonProperty("neighborhood")
    private String neighborhood;
    @JsonProperty("phone")
    private List<String> phone;
    @JsonProperty("email")
    private String email;
    @JsonProperty("url")
    private String url;
    @JsonProperty("image_attribution")
    private ImageAttribution imageAttribution;
    @JsonProperty("nearest_train")
    private String nearestTrain;
    @JsonProperty("nearest_bus")
    private String nearestBus;
    @JsonProperty("credit_card")
    private List<CreditCard> creditCard = null;
    @JsonProperty("eds_choice")
    private String edsChoice;
    @JsonProperty("imagefile")
    private String imagefile;
    @JsonProperty("fax")
    private String fax;
    @JsonProperty("disabled_access")
    private String disabledAccess;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("id")
    public String getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(String id) {
        this.id = id;
    }

    @JsonProperty("distance")
    public String getDistance() {
        return distance;
    }

    @JsonProperty("distance")
    public void setDistance(String distance) {
        this.distance = distance;
    }

    @JsonProperty("category")
    public List<Category> getCategory() {
        return category;
    }

    @JsonProperty("category")
    public void setCategory(List<Category> category) {
        this.category = category;
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty("short_desc")
    public String getShortDesc() {
        return shortDesc;
    }

    @JsonProperty("short_desc")
    public void setShortDesc(String shortDesc) {
        this.shortDesc = shortDesc;
    }

    @JsonProperty("long_desc")
    public String getLongDesc() {
        return longDesc;
    }

    @JsonProperty("long_desc")
    public void setLongDesc(String longDesc) {
        this.longDesc = longDesc;
    }

    @JsonProperty("booking_url")
    public List<String> getBookingUrl() {
        return bookingUrl;
    }

    @JsonProperty("booking_url")
    public void setBookingUrl(List<String> bookingUrl) {
        this.bookingUrl = bookingUrl;
    }

    @JsonProperty("address")
    public Address getAddress() {
        return address;
    }

    @JsonProperty("address")
    public void setAddress(Address address) {
        this.address = address;
    }

    @JsonProperty("neighborhood")
    public String getNeighborhood() {
        return neighborhood;
    }

    @JsonProperty("neighborhood")
    public void setNeighborhood(String neighborhood) {
        this.neighborhood = neighborhood;
    }

    @JsonProperty("phone")
    public List<String> getPhone() {
        return phone;
    }

    @JsonProperty("phone")
    public void setPhone(List<String> phone) {
        this.phone = phone;
    }

    @JsonProperty("email")
    public String getEmail() {
        return email;
    }

    @JsonProperty("email")
    public void setEmail(String email) {
        this.email = email;
    }

    @JsonProperty("url")
    public String getUrl() {
        return url;
    }

    @JsonProperty("url")
    public void setUrl(String url) {
        this.url = url;
    }

    @JsonProperty("image_attribution")
    public ImageAttribution getImageAttribution() {
        return imageAttribution;
    }

    @JsonProperty("image_attribution")
    public void setImageAttribution(ImageAttribution imageAttribution) {
        this.imageAttribution = imageAttribution;
    }

    @JsonProperty("nearest_train")
    public String getNearestTrain() {
        return nearestTrain;
    }

    @JsonProperty("nearest_train")
    public void setNearestTrain(String nearestTrain) {
        this.nearestTrain = nearestTrain;
    }

    @JsonProperty("nearest_bus")
    public String getNearestBus() {
        return nearestBus;
    }

    @JsonProperty("nearest_bus")
    public void setNearestBus(String nearestBus) {
        this.nearestBus = nearestBus;
    }

    @JsonProperty("credit_card")
    public List<CreditCard> getCreditCard() {
        return creditCard;
    }

    @JsonProperty("credit_card")
    public void setCreditCard(List<CreditCard> creditCard) {
        this.creditCard = creditCard;
    }

    @JsonProperty("eds_choice")
    public String getEdsChoice() {
        return edsChoice;
    }

    @JsonProperty("eds_choice")
    public void setEdsChoice(String edsChoice) {
        this.edsChoice = edsChoice;
    }

    @JsonProperty("imagefile")
    public String getImagefile() {
        return imagefile;
    }

    @JsonProperty("imagefile")
    public void setImagefile(String imagefile) {
        this.imagefile = imagefile;
    }

    @JsonProperty("fax")
    public String getFax() {
        return fax;
    }

    @JsonProperty("fax")
    public void setFax(String fax) {
        this.fax = fax;
    }

    @JsonProperty("disabled_access")
    public String getDisabledAccess() {
        return disabledAccess;
    }

    @JsonProperty("disabled_access")
    public void setDisabledAccess(String disabledAccess) {
        this.disabledAccess = disabledAccess;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
