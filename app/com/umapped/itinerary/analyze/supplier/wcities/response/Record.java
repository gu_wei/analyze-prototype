
package com.umapped.itinerary.analyze.supplier.wcities.response;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "details",
    "accommodation_type",
    "check_in",
    "check_out",
    "price_range",
    "breakfast",
    "hotel_services",
    "number_of_rooms",
    "part_of_chain",
    "conferences"
})
public class Record {

    @JsonProperty("details")
    private Details details;
    @JsonProperty("accommodation_type")
    private String accommodationType;
    @JsonProperty("check_in")
    private String checkIn;
    @JsonProperty("check_out")
    private String checkOut;
    @JsonProperty("price_range")
    private PriceRange priceRange;
    @JsonProperty("breakfast")
    private String breakfast;
    @JsonProperty("hotel_services")
    private List<HotelService> hotelServices = null;
    @JsonProperty("number_of_rooms")
    private String numberOfRooms;
    @JsonProperty("part_of_chain")
    private String partOfChain;
    @JsonProperty("conferences")
    private String conferences;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("details")
    public Details getDetails() {
        return details;
    }

    @JsonProperty("details")
    public void setDetails(Details details) {
        this.details = details;
    }

    @JsonProperty("accommodation_type")
    public String getAccommodationType() {
        return accommodationType;
    }

    @JsonProperty("accommodation_type")
    public void setAccommodationType(String accommodationType) {
        this.accommodationType = accommodationType;
    }

    @JsonProperty("check_in")
    public String getCheckIn() {
        return checkIn;
    }

    @JsonProperty("check_in")
    public void setCheckIn(String checkIn) {
        this.checkIn = checkIn;
    }

    @JsonProperty("check_out")
    public String getCheckOut() {
        return checkOut;
    }

    @JsonProperty("check_out")
    public void setCheckOut(String checkOut) {
        this.checkOut = checkOut;
    }

    @JsonProperty("price_range")
    public PriceRange getPriceRange() {
        return priceRange;
    }

    @JsonProperty("price_range")
    public void setPriceRange(PriceRange priceRange) {
        this.priceRange = priceRange;
    }

    @JsonProperty("breakfast")
    public String getBreakfast() {
        return breakfast;
    }

    @JsonProperty("breakfast")
    public void setBreakfast(String breakfast) {
        this.breakfast = breakfast;
    }

    @JsonProperty("hotel_services")
    public List<HotelService> getHotelServices() {
        return hotelServices;
    }

    @JsonProperty("hotel_services")
    public void setHotelServices(List<HotelService> hotelServices) {
        this.hotelServices = hotelServices;
    }

    @JsonProperty("number_of_rooms")
    public String getNumberOfRooms() {
        return numberOfRooms;
    }

    @JsonProperty("number_of_rooms")
    public void setNumberOfRooms(String numberOfRooms) {
        this.numberOfRooms = numberOfRooms;
    }

    @JsonProperty("part_of_chain")
    public String getPartOfChain() {
        return partOfChain;
    }

    @JsonProperty("part_of_chain")
    public void setPartOfChain(String partOfChain) {
        this.partOfChain = partOfChain;
    }

    @JsonProperty("conferences")
    public String getConferences() {
        return conferences;
    }

    @JsonProperty("conferences")
    public void setConferences(String conferences) {
        this.conferences = conferences;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
