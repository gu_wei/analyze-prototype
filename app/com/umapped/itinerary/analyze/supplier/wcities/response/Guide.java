
package com.umapped.itinerary.analyze.supplier.wcities.response;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "guideid",
    "guideName",
    "editorial"
})
public class Guide {

    @JsonProperty("guideid")
    private String guideid;
    @JsonProperty("guideName")
    private String guideName;
    @JsonProperty("editorial")
    private String editorial;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("guideid")
    public String getGuideid() {
        return guideid;
    }

    @JsonProperty("guideid")
    public void setGuideid(String guideid) {
        this.guideid = guideid;
    }

    @JsonProperty("guideName")
    public String getGuideName() {
        return guideName;
    }

    @JsonProperty("guideName")
    public void setGuideName(String guideName) {
        this.guideName = guideName;
    }

    @JsonProperty("editorial")
    public String getEditorial() {
        return editorial;
    }

    @JsonProperty("editorial")
    public void setEditorial(String editorial) {
        this.editorial = editorial;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
