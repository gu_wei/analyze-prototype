
package com.umapped.itinerary.analyze.supplier.wcities.response;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "bookinglink",
    "booking_no"
})
public class Bookinginfo {

    @JsonProperty("bookinglink")
    private List<Bookinglink> bookinglink;
    @JsonProperty("booking_no")
    private String bookingNo;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("bookinglink")
    public List<Bookinglink> getBookinglink() {
        return bookinglink;
    }

    @JsonProperty("bookinglink")
    public void setBookinglink(List<Bookinglink> bookinglink) {
        this.bookinglink = bookinglink;
    }

    @JsonProperty("booking_no")
    public String getBookingNo() {
        return bookingNo;
    }

    @JsonProperty("booking_no")
    public void setBookingNo(String bookingNo) {
        this.bookingNo = bookingNo;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
