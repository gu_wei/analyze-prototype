
package com.umapped.itinerary.analyze.supplier.wcities.response;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "single_from",
    "double_from",
    "weekend_rate",
    "suite_from",
    "triple_from"
})
public class PriceRange {

    @JsonProperty("single_from")
    private String singleFrom;
    @JsonProperty("double_from")
    private String doubleFrom;
    @JsonProperty("weekend_rate")
    private String weekendRate;
    @JsonProperty("suite_from")
    private String suiteFrom;
    @JsonProperty("triple_from")
    private String tripleFrom;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("single_from")
    public String getSingleFrom() {
        return singleFrom;
    }

    @JsonProperty("single_from")
    public void setSingleFrom(String singleFrom) {
        this.singleFrom = singleFrom;
    }

    @JsonProperty("double_from")
    public String getDoubleFrom() {
        return doubleFrom;
    }

    @JsonProperty("double_from")
    public void setDoubleFrom(String doubleFrom) {
        this.doubleFrom = doubleFrom;
    }

    @JsonProperty("weekend_rate")
    public String getWeekendRate() {
        return weekendRate;
    }

    @JsonProperty("weekend_rate")
    public void setWeekendRate(String weekendRate) {
        this.weekendRate = weekendRate;
    }

    @JsonProperty("suite_from")
    public String getSuiteFrom() {
        return suiteFrom;
    }

    @JsonProperty("suite_from")
    public void setSuiteFrom(String suiteFrom) {
        this.suiteFrom = suiteFrom;
    }

    @JsonProperty("triple_from")
    public String getTripleFrom() {
        return tripleFrom;
    }

    @JsonProperty("triple_from")
    public void setTripleFrom(String tripleFrom) {
        this.tripleFrom = tripleFrom;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
