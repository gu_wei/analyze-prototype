package com.umapped.itinerary.analyze.supplier.wcities;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.inject.Guice;
import com.google.inject.Injector;
import com.umapped.itinerary.analyze.ItineraryAnalyzeModule;
import com.umapped.itinerary.analyze.WebServiceModule;
import com.umapped.itinerary.analyze.model.Location;
import com.umapped.itinerary.analyze.model.StayPeriod;
import com.umapped.itinerary.analyze.recommendation.RecommendedItem;
import com.umapped.itinerary.analyze.supplier.Supplier;
import com.umapped.itinerary.analyze.supplier.wcities.response.*;
import com.umapped.itinerary.analyze.tests.EBeanServerConfig;
import com.umapped.itinerary.analyze.webservice.WebAPIServiceRequest;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;

/**
 * Created by wei on 2017-03-30.
 */
@Singleton public class WCitiesWebService
    implements Constants {
  private static final Logger LOG = LoggerFactory.getLogger(WCitiesWebService.class);

  private ObjectMapper objectMapper;
  private final WCitiesWebServiceRequestHandler requestHandler;
  private final WCitiesCityLookup cityLookup;


  @Inject public WCitiesWebService(ObjectMapper objectMapper) {
    this.objectMapper = objectMapper;
    cityLookup = new WCitiesCityLookup();
    requestHandler = new WCitiesWebServiceRequestHandler(objectMapper);
  }

  private WebAPIServiceRequest newRequest(Location.GeoLocation geoLocation, int radius) {
    WebAPIServiceRequest request = new WebAPIServiceRequest();
    request.addParam(OAUTH_TOKEN, API_KEY);
    request.addParam(LATITUDE, Double.toString(geoLocation.getLatitude()));
    request.addParam(LONGITUDE, Double.toString(geoLocation.getLongitude()));
    request.addParam(RADIUS_IN_MILE, Integer.toString(radius));
    return request;
  }

  private WebAPIServiceRequest newRequest(String cityId) {
    WebAPIServiceRequest request = new WebAPIServiceRequest();
    request.addParam(OAUTH_TOKEN, API_KEY);
    request.addParam(CITY_ID, cityId);

    return request;
  }

  public List<RecommendedItem> getGuides(Location location) {
    if (location != null) {
      String cityId = cityLookup.getCityId(location.getGeoLocation());
      if (StringUtils.isNotEmpty(cityId)) {
        WebAPIServiceRequest request = newRequest(cityId);
        CityGuideResponse response = requestHandler.get(GET_GUIDES, request, CityGuideResponse.class);

        List<Guide> guides = response.getCityguides().getGuide();
        List<RecommendedItem> contents = new ArrayList<>();
        for (Guide g : guides) {
          RecommendedItem gc = new RecommendedItem();
          gc.type = RecommendedItem.Type.Content;
          gc.name = g.getGuideName();
          gc.supplier = Supplier.WCities;
          gc.description = g.getEditorial();
          contents.add(gc);
        }
        return contents;
      }
    } return Collections.emptyList();
  }

  public List<RecommendedItem> getEvents(StayPeriod period, Location location, RequestPreset preset) {
    Location.GeoLocation geoLocation = location.getGeoLocation();
    if (geoLocation == null) {
      return Collections.emptyList();
    }

    WebAPIServiceRequest request = newRequest(geoLocation, 30);

    if (period.getStart() != null) {
      request.addParam(START_DATE, getDateString(period.getStart()));
    }
    if (period.getEnd() != null) {
      request.addParam(END_DATE, getDateString(period.getEnd()));
    }
    request.addParam(LIMIT, "0,5");
    addRequestPresets(request, preset);

    GetEventResponse response = requestHandler.get(GET_EVENTS, request, GetEventResponse.class);

    if (response.getCityevent().getEvents() != null) {
      List<Event> eventList = response.getCityevent().getEvents().getEvent();
      List<RecommendedItem> rEvents = eventList.stream().map(event -> {
        RecommendedItem wEvent = new RecommendedItem();
        wEvent.supplier = Supplier.WCities;
        wEvent.type = RecommendedItem.Type.Activity;
        wEvent.id = event.getId();
        wEvent.name = event.getName();
        wEvent.description = StringUtils.replace(event.getDesc(), "\u2028", "");


        try {
          populateVenueInformation(wEvent, response.getCityevent().getVenues(), event.getSchedule().get(0).getVenueId());
          Bookinglink bookingLink = event.getSchedule().get(0).getBookinginfo().getBookinglink().get(0);
          wEvent.bookingUrl = bookingLink.getBookingUrl();
          if (bookingLink.getPrice() != null) {
            wEvent.prices = new ArrayList<>();
            wEvent.prices.add(new RecommendedItem.Price(bookingLink.getPrice().getValue(),
                                                        bookingLink.getPrice().getCurrency(),
                                                        ""));
          }
        }
        catch (NullPointerException e) {
        }
        return wEvent;
      }).collect(Collectors.toList());
      return rEvents;
    }
    return Collections.emptyList();
  }


  private void populateVenueInformation(RecommendedItem wEvent, Venues venues, String venueId) {
    if (venues != null && venues.getVenue() != null) {
      for (Venue v : venues.getVenue()) {
        if (v.getId().equals(venueId)) {
          if (v.getImagefile() != null) {
            wEvent.imageUrl = "http://c0056904.cdn2.cloudfiles.rackspacecloud.com/" + v.getImagefile();
          }
          wEvent.address = extractAddress(v.getAddress());
        }
      }
    }
  }

  public List<RecommendedItem> getHotels(WebAPIServiceRequest request) {
    addRequestPresets(request, RequestPreset.Hotel);

    request.addParam(LIMIT, "0,5");
    GetRecordResponse response = requestHandler.get(GET_RECORDS, request, GetRecordResponse.class);

    if (response.getRecords() != null) {
      List<Record> recordList = response.getRecords().getRecord();
      if (recordList != null) {
        List<RecommendedItem> hotels = recordList.stream().map(r -> {
          RecommendedItem hotel = new RecommendedItem();
          hotel.supplier = Supplier.WCities;
          hotel.type = RecommendedItem.Type.Accommodation;
          hotel.id = r.getDetails().getId();
          hotel.name = r.getDetails().getName();
          hotel.description = r.getDetails().getLongDesc();
          hotel.bookingUrl = CollectionUtils.isEmpty(r.getDetails().getBookingUrl()) ?
                             null :
                             r.getDetails().getBookingUrl().get(0);
          if (StringUtils.isNotEmpty(r.getDetails().getImagefile())) {
            hotel.imageUrl = "http://c0056904.cdn2.cloudfiles.rackspacecloud.com/" + r.getDetails().getImagefile();
          }
          hotel.address = extractAddress(r.getDetails().getAddress());
          hotel.prices = extractPrices(r.getPriceRange());
          return hotel;
        }).collect(Collectors.toList());
        return hotels;
      }
    }
    return Collections.emptyList();
  }

  private RecommendedItem.Address extractAddress(Address location) {
    RecommendedItem.Address address = null;
    if (location !=null) {
      address = new RecommendedItem.Address();
      if (StringUtils.isEmpty(location.getAddress2())) {
        address.streetAddress = location.getAddress1();
      } else {
        address.streetAddress = location.getAddress1() + "," + location.getAddress2();
      }
      address.locality = StringUtils.trimToEmpty(location.getCity());
      address.region = StringUtils.trimToEmpty(location.getState());
      if (location.getCountry()!=null) {
        address.country = StringUtils.trimToEmpty(location.getCountry().getName());
      }
    }
    return address;
  }

  private List<RecommendedItem.Price> extractPrices(PriceRange priceRange) {
    List<RecommendedItem.Price> prices = null;
    if (priceRange != null) {
      prices = new ArrayList<>();
      if (priceRange.getSingleFrom()!=null) {
        prices.add(new RecommendedItem.Price(formatPriceValue(priceRange.getSingleFrom()), "", "Single room"));
      }
      if (priceRange.getDoubleFrom()!=null) {
        prices.add(new RecommendedItem.Price(formatPriceValue(priceRange.getDoubleFrom()), "", "Double room"));
      }
      if (priceRange.getSuiteFrom()!=null) {
        prices.add(new RecommendedItem.Price(formatPriceValue(priceRange.getSuiteFrom()), "", "Suite"));
      }
    }
    return prices;
  }

  private String formatPriceValue(String priceValue) {
    try {
      StringBuilder builder = new StringBuilder();
      String[] values = priceValue.split("-");
      builder.append(String.format("%.2f", Float.parseFloat(values[0])));
      if (values.length == 2) {
        builder.append(" - ");
        builder.append(String.format("%.2f", Float.parseFloat(values[1])));
      }

      return builder.toString();
    } catch (Exception e) {
      // can't parse it
      return priceValue;
    }
  }

  public List<RecommendedItem> getHotelsByCity(String cityId) {
    if (StringUtils.isEmpty(cityId)) {
      return Collections.emptyList();
    }

    WebAPIServiceRequest request = newRequest(cityId);
    return getHotels(request);
  }

  public List<RecommendedItem> getHotelsByLocation(Location location) {
    Location.GeoLocation geoLocation = location.getGeoLocation();
    if (geoLocation == null) {
      return Collections.emptyList();
    }

    WebAPIServiceRequest request = newRequest(geoLocation, 20);

    return getHotels(request);
  }

  private void addRequestPresets(WebAPIServiceRequest request, RequestPreset preset) {
    if (preset != null) {
      for (String k : preset.getKeys()) {
        request.addParam(k, preset.getValue(k));
      }
    }
  }

  private String getDateString(LocalDate date) {
    return DateTimeFormatter.ISO_DATE.format(date);
  }


  public Map<String, List<RecommendedItem>> getHotels(Location location) {
    Map<String, List<RecommendedItem>> hotelMap = new HashMap<>();

    CompletableFuture<List<RecommendedItem>> cityHotels = CompletableFuture.supplyAsync(() -> cityLookup
        .getCityId(
        location.getGeoLocation())).thenApply(cityId -> getHotelsByCity(cityId));

    CompletableFuture<List<RecommendedItem>> nearByHotels = CompletableFuture.supplyAsync(() ->
                                                                                                       getHotelsByLocation(
        location));

    cityHotels.thenAcceptBoth(nearByHotels, (c, n) -> {
      hotelMap.put(FEATURE_CITY, c);
      hotelMap.put(FEATURE_NEARBY, n);
    }).join();

    return hotelMap;
  }

 }
