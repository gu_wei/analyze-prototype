package com.umapped.itinerary.analyze.supplier;

import com.umapped.itinerary.analyze.TripFeature;
import com.umapped.itinerary.analyze.segment.TripSegment;
import com.umapped.itinerary.analyze.segment.decisiontree.DecisionResult;
import com.umapped.itinerary.analyze.segment.feature.SegmentFeature;

import java.util.EnumMap;

/**
 * Created by wei on 2017-04-20.
 */
public interface ProviderRepository {
  AccommondationProvider getAccommodationProvider(DecisionResult decisionResult,
                                                  TripSegment segment,
                                                  EnumMap<TripFeature, Object> tripFeatures,
                                                  EnumMap<SegmentFeature, Object> segmentFeatures);

  ActivityProvider getActivityProvider(DecisionResult decisionResult,
                                       TripSegment segment,
                                       EnumMap<TripFeature, Object> tripFeatures,
                                       EnumMap<SegmentFeature, Object> segmentFeatures);

  ContentProvider getContentProvider(DecisionResult decisionResult,
                                     TripSegment segment,
                                     EnumMap<TripFeature, Object> tripFeatures,
                                     EnumMap<SegmentFeature, Object> segmentFeatures);
}
