package controllers;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mapped.publisher.parse.schemaorg.ReservationPackage;
import com.umapped.itinerary.analyze.ItineraryAnalyzeResult;
import com.umapped.itinerary.analyze.recommendation.ItineraryRecommendation;
import com.umapped.itinerary.analyze.recommendation.RecommendationService;
import play.libs.Json;
import play.mvc.BodyParser;
import play.mvc.Controller;
import play.mvc.Result;

import javax.inject.Inject;

/**
 * Created by wei on 2017-04-05.
 */
public class ItineraryAnalyzeService extends Controller {

  @Inject ObjectMapper objectMapper;

  @Inject com.umapped.itinerary.analyze.ItineraryAnalyzeService itineraryAnalyzeService;

  @Inject RecommendationService recommendationService;

  @BodyParser.Of(BodyParser.Json.class)
  public Result analyze() {
    JsonNode jsonRequest = request().body().asJson();

    ReservationPackage rp = objectMapper.convertValue(jsonRequest, ReservationPackage.class);

    ItineraryAnalyzeResult result = itineraryAnalyzeService.analyze(rp);

    return ok(Json.toJson(result));
  }

  @BodyParser.Of(BodyParser.Json.class)
  public Result recommend() {
    JsonNode jsonRequest = request().body().asJson();

    ReservationPackage rp = objectMapper.convertValue(jsonRequest, ReservationPackage.class);

    ItineraryAnalyzeResult result = itineraryAnalyzeService.analyze(rp);

    ItineraryRecommendation recommendation = recommendationService.recommend(result);
    return ok(Json.toJson(recommendation.getRecommendationByPeriod()));
  }
}
