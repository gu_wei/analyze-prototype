package com.umapped.itinerary.analyze.segment;

import com.umapped.itinerary.analyze.model.ItineraryItem;
import com.umapped.itinerary.analyze.model.StayPeriod;
import com.umapped.itinerary.analyze.segment.feature.SegmentFeatureDetectorCollection;
import org.junit.Assert;
import org.junit.Test;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

/**
 * Created by wei on 2017-03-10.
 */
public class TestSegmentDetectorCollection {

  @Test
  public void testAccommodationStay() {
    TripSegment segment = new TripSegment();

    segment.setStartDateTime(ZonedDateTime.of(LocalDateTime.parse("2017-03-01T15:00:00", DateTimeFormatter.ISO_DATE_TIME),
                             ZoneId.systemDefault()));

    segment.setEndDateTime(ZonedDateTime.of(LocalDateTime.parse("2017-03-06T12:00:00", DateTimeFormatter.ISO_DATE_TIME),
                                              ZoneId.systemDefault()));

    List<StayPeriod> stays = SegmentFeatureDetectorCollection.daysWithoutAccommodation(segment);

    Assert.assertEquals(1, stays.size());
    Assert.assertEquals(LocalDate.of(2017, 3, 1), stays.get(0).getStart());
    Assert.assertEquals(LocalDate.of(2017, 3, 5), stays.get(0).getEnd());

    ItineraryItem hotel = new ItineraryItem();
    hotel.setStartDateTime(ZonedDateTime.of(LocalDateTime.parse("2017-03-02T15:00:00", DateTimeFormatter.ISO_DATE_TIME),
                                            ZoneId.systemDefault()));
    hotel.setEndDateTime(ZonedDateTime.of(LocalDateTime.parse("2017-03-04T15:00:00", DateTimeFormatter.ISO_DATE_TIME),
                                            ZoneId.systemDefault()));

    segment.addAccommodation(hotel);

    StayPeriod hotelStay = SegmentFeatureDetectorCollection.getAccomondationStayPeriod(segment);
    Assert.assertEquals(LocalDate.of(2017, 3, 2), hotelStay.getStart());
    Assert.assertEquals(LocalDate.of(2017, 3, 3), hotelStay.getEnd());

    stays = SegmentFeatureDetectorCollection.daysWithoutAccommodation(segment);

    Assert.assertEquals(2, stays.size());
    Assert.assertEquals(LocalDate.of(2017, 3, 1), stays.get(0).getStart());
    Assert.assertEquals(LocalDate.of(2017, 3, 1), stays.get(0).getEnd());
    Assert.assertEquals(LocalDate.of(2017, 3, 4), stays.get(1).getStart());
    Assert.assertEquals(LocalDate.of(2017, 3, 5), stays.get(1).getEnd());

    hotel.setStartDateTime(ZonedDateTime.of(LocalDateTime.parse("2017-03-01T15:00:00", DateTimeFormatter.ISO_DATE_TIME),
                                            ZoneId.systemDefault()));
    hotel.setEndDateTime(ZonedDateTime.of(LocalDateTime.parse("2017-03-06T15:00:00", DateTimeFormatter.ISO_DATE_TIME),
                                          ZoneId.systemDefault()));

    stays = SegmentFeatureDetectorCollection.daysWithoutAccommodation(segment);

    Assert.assertEquals(0, stays.size());

  }

  @Test
  public void testActivityGaps() {
    TripSegment segment = new TripSegment();
    segment.setStartDateTime(ZonedDateTime.of(LocalDateTime.parse("2017-03-01T15:00:00", DateTimeFormatter.ISO_DATE_TIME),
                                              ZoneId.systemDefault()));

    segment.setEndDateTime(ZonedDateTime.of(LocalDateTime.parse("2017-03-08T12:00:00", DateTimeFormatter.ISO_DATE_TIME),
                                            ZoneId.systemDefault()));
    List<StayPeriod> periods = SegmentFeatureDetectorCollection.daysWithoutActivites(segment);

    Assert.assertEquals(1, periods.size());
    Assert.assertEquals(LocalDate.of(2017, 3, 2), periods.get(0).getStart());
    Assert.assertEquals(LocalDate.of(2017, 3, 7), periods.get(0).getEnd());

    ItineraryItem event1 = new ItineraryItem();
    event1.setStartDateTime(ZonedDateTime.of(LocalDateTime.parse("2017-03-03T15:00:00", DateTimeFormatter.ISO_DATE_TIME),
                                             ZoneId.systemDefault()))
          .setEndDateTime(ZonedDateTime.of(LocalDateTime.parse("2017-03-03T15:00:00", DateTimeFormatter.ISO_DATE_TIME),
                                           ZoneId.systemDefault()));
    segment.addActivity(event1);
    periods = SegmentFeatureDetectorCollection.daysWithoutActivites(segment);
    Assert.assertEquals(2, periods.size());
    Assert.assertEquals(LocalDate.of(2017, 3, 2), periods.get(0).getStart());
    Assert.assertEquals(LocalDate.of(2017, 3, 2), periods.get(0).getEnd());
    Assert.assertEquals(LocalDate.of(2017, 3, 4), periods.get(1).getStart());
    Assert.assertEquals(LocalDate.of(2017, 3, 7), periods.get(1).getEnd());

    ItineraryItem event2 = new ItineraryItem();
    event2.setStartDateTime(ZonedDateTime.of(LocalDateTime.parse("2017-03-05T15:00:00", DateTimeFormatter.ISO_DATE_TIME),
                                             ZoneId.systemDefault()));
    segment.addActivity(event2);

    periods = SegmentFeatureDetectorCollection.daysWithoutActivites(segment);
    Assert.assertEquals(2, periods.size());
    Assert.assertEquals(LocalDate.of(2017, 3, 2), periods.get(0).getStart());
    Assert.assertEquals(LocalDate.of(2017, 3, 2), periods.get(0).getEnd());
    Assert.assertEquals(LocalDate.of(2017, 3, 4), periods.get(1).getStart());
    Assert.assertEquals(LocalDate.of(2017, 3, 4), periods.get(1).getEnd());

  }
}
