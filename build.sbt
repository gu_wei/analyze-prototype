name := """analyzer-service"""

version := "1.0-SNAPSHOT"

lazy val root = (project in file(".")).enablePlugins(PlayJava, PlayEbean)

scalaVersion := "2.11.8"

libraryDependencies ++= Seq(
  javaJdbc,
  cache,
  javaWs,
  filters,
  // Umapped project dependencies:
  "org.avaje.ebean" % "ebean" % "9.2.1",
  "org.avaje.ebean" % "ebean-postgis" % "0.1.1",        //PostGIS support is currently a separate asset, specifies all dependencies
  "org.postgresql" % "postgresql" % "9.4.1212",
  "com.amazonaws" % "aws-java-sdk-s3" % "1.11.55",      //updated to s3 sdk only
  "com.amazonaws" % "aws-java-sdk-sqs" % "1.11.55",     //updated to sqs sdk only

  "org.jsoup" % "jsoup" % "1.10.1",
  "org.apache.httpcomponents" % "httpcomponents-core" % "4.4.5",
  //JWT Tokens library
  "io.jsonwebtoken" % "jjwt" % "0.7.0",
  //Log Entries appender
  "com.logentries" % "logentries-appender" % "1.1.35",
  "com.umapped" % "apischema" % "0.1",
  "commons-collections" % "commons-collections" % "3.2.2",
  "com.google.maps" % "google-maps-services" % "0.1.17"
)

includeFilter in (Assets, LessKeys.less) := "*.less"
excludeFilter in (Assets, LessKeys.less) := "_*.less"

TwirlKeys.templateImports += "org.apache.commons.lang3.StringEscapeUtils.escapeEcmaScript"

resolvers += "UMapped Maven Repo Releases" at "https://api.bitbucket.org/1.0/repositories/umapped/mavenrepo.git/raw/releases"
resolvers += "UMapped Maven Repo Snapshots" at "https://api.bitbucket.org/1.0/repositories/umapped/mavenrepo.git/raw/snapshots"

resolvers += "Local Maven Repository" at "file://"+Path.userHome.absolutePath+"/.m2/repository"

//credentials += Credentials(Path.userHome / ".ivy2" / ".credentials")
val bitbucketUserId = sys.env.getOrElse("bitbucketUserId", "");
val bitbucketPwd = sys.env.getOrElse("bitbucketPwd", "");
credentials += Credentials("Bitbucket.org HTTP", "api.bitbucket.org", bitbucketUserId , bitbucketPwd);

//javacOptions ++= Seq("-Xlint:unchecked")
//javacOptions += "-Xlint:deprecation"

routesGenerator := InjectedRoutesGenerator
playEnhancerEnabled := true

//Debugging in test - Disabling forking
Keys.fork in Test := false
//Debugging in test  - setting up socket parameters
//Keys.javaOptions in (Test) += "-Xrunjdwp:transport=dt_socket,server=y,suspend=n,address=9999"
//Disabling parallel exectuion
parallelExecution in Test := false

//fixed depency thast was broken by some ebean module to 1.8 alpha
dependencyOverrides += "org.slf4j" % "slf4j-api" % "1.7.25"